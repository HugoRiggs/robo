﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class RightBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData ped) {
        GameObject.FindObjectOfType<PlayerMovement>().moveRightConditional(true);
    }

    public void OnPointerUp(PointerEventData ped) {
        GameObject.FindObjectOfType<PlayerMovement>().moveRightConditional(false);
    }
}
