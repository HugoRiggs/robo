﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingBarScript : MonoBehaviour {

    AsyncOperation ao;
    public GameObject loadingScreenBG;
    public Slider progBar;
    public Text loadingText;

    public void LoadLevel(int levelNumber) {
        Time.timeScale = (0);                                   // pause game
        loadingScreenBG.SetActive(true);                        // show background
        progBar.gameObject.SetActive(true);                     // show loading bar
        loadingText.text = "Loading ...";                       // set text
        loadingText.gameObject.SetActive(true);                 // display text
        StartCoroutine(LoadLevelWithProgress(levelNumber));     // call asynchronous method 
    }
    
    IEnumerator LoadLevelWithProgress(int n) {
        ao = SceneManager.LoadSceneAsync(n);
        ao.allowSceneActivation = false;
        while (!ao.isDone) {
            progBar.value = ao.progress;
            ao.allowSceneActivation = true;
            yield return null;
        }
        progBar.value = progBar.maxValue; 
        Time.timeScale=(1);
    }

    IEnumerator LoadSaveWithProgress(int n) {
        ao = SceneManager.LoadSceneAsync(n);
        ao.allowSceneActivation = false;
        while (!ao.isDone) {
            progBar.value = ao.progress;
            ao.allowSceneActivation = true;
            yield return null;
        }
        progBar.value = progBar.maxValue;
        Time.timeScale = (1);
    }

}
