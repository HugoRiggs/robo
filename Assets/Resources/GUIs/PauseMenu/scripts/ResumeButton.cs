﻿using UnityEngine;
using System.Collections;

public class ResumeButton : MonoBehaviour {


	private PauseMenu pauseMenu;
	public void Start () {
		pauseMenu=GameObject.FindGameObjectWithTag("Pause Menu").GetComponent<PauseMenu>();
	}


	public void onButtonPressed(){
		pauseMenu.UnPause();
	}
	

}
