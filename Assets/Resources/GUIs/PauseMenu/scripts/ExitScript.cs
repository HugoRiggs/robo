﻿/*
 * This script will be used for the "exit", and then the "are you sure you want to quit" buttons
 */

using UnityEngine;
using System.Collections;

public class ExitScript : MonoBehaviour {

    public GameObject Target;


    public void confirmChoice(GameObject Target) {
        Target.SetActive(true);
        gameObject.SetActive(false);
    }


    public void quitGame() {
        print("game has quit");
        Application.Quit();
    }
}
