﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class PauseMenu : MonoBehaviour {


	public bool paused;
	public GameObject ToggleableContainer; 

	private Button[] buttons;
	private bool showGUI;


	void Start () {
		paused = false;
		showGUI = false;
		ToggleableContainer.SetActive(false);
		Time.timeScale = 1.0f;
		//ResumeButton.onClick.AddListener (delegate { UnPause(); });
		buttons = this.GetComponentsInChildren<Button>();
	}


	public void UnPause(){ 
		paused = false;
		showGUI = false; 
	}


	// Update is called once per frame
	//  TODO: can this be done without a void update
	void Update () {

		
		if (!paused) {
			showGUI = false;
		}

		if (paused) {
			showGUI = true;
		}

		if (showGUI) {
			ToggleableContainer.SetActive (true);
			Time.timeScale = 0;
		} 
		else {
			ToggleableContainer.SetActive (false);
			Time.timeScale = 1f;
		}

		if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7"))) {
			if (!paused) {
				paused = true;
			}
				else {
				paused = false;
			}
		}

    }
}
