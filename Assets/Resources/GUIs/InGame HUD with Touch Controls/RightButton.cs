﻿/*
 * Input to move right for cell phones (GUI Button).
 */

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class RightButton : EventTrigger {

private PlayerMovement pl;
    private bool playerSet = false;
    private TimeDependentFunctionCalling timeDep;

    public void Start() { StartCoroutine(getPlayerThenStopUpdate()); }

    private void moveRight(bool turnOn) { pl.SendMessage("moveRightConditional", turnOn); }

    public override void OnPointerDown(PointerEventData eventData) { moveRight(true); }

    public override void OnPointerUp(PointerEventData eventData) { moveRight(false); }


    IEnumerator getPlayerThenStopUpdate() {
        while (!playerSet) {
            try {
                pl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
                playerSet = true;
            } catch (Exception e) { print("Exception " + e); }
            yield return new WaitForEndOfFrame();
        }

    }}
