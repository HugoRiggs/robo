﻿/*
 * Move player left touch screen controls. (Buton)
 * Writing convention: functions with single line blocks are inline.
 */

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class LeftButton : EventTrigger {

private PlayerMovement pl;
    private bool playerSet = false;
    private TimeDependentFunctionCalling timeDep;

    public void Start() { StartCoroutine(getPlayerThenStopUpdate()); }

    private void move(bool turnOn) { pl.SendMessage("moveLeftConditional", turnOn); }

    public override void OnPointerDown(PointerEventData eventData) { move(true); }

    public override void OnPointerUp(PointerEventData eventData) { move(false); }


    IEnumerator getPlayerThenStopUpdate() {
        while (!playerSet) {
            try {
                pl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
                playerSet = true;
            } catch (Exception e) { print("Exception " + e); }
            yield return new WaitForEndOfFrame();
        }

    }}
