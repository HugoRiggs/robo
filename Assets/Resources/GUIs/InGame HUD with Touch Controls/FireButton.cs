﻿/*
 * Part of Cell Phone Controlls.
 * This script looks for weapons system.
 */


using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;


public class FireButton : EventTrigger {

    private WeaponsSystem ws;  // Declare a weapons system object
    private bool weaponSystemSet = false;  // system is not initialized

    public void Start() { StartCoroutine(getPlayerThenStopUpdate()); }


    public void fire() { ws.SendMessage("fire"); } 

    // Search for a player, when found get their weapons system and stop searching.
    IEnumerator getPlayerThenStopUpdate() {
        while (!weaponSystemSet) { 
            try {
                ws = GameObject.FindGameObjectWithTag("Player").GetComponent<WeaponsSystem>();
                weaponSystemSet = true;
            } catch (Exception e) { print("Exception " + e); }
            yield return new WaitForEndOfFrame();
        }
    }



}
