﻿/*
 * GUI button to move upwards (jump/jetpack) for cell phones.
 */

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class UpButtonScript : EventTrigger {

    private PlayerMovement pl;
    private bool playerSet = false;
    private TimeDependentFunctionCalling timeDep;

    public void Start() { StartCoroutine(getPlayerThenStopUpdate()); }

    private void doJump() { pl.SendMessage("doJump"); }

    private void jetPackOn(bool turnOn) {
        pl.SendMessage("setJetPackOn", turnOn);
        //pl.SendMessage("useJet_pack");
    }

    public override void OnPointerDown(PointerEventData eventData) {
        //StartCoroutine(timeDep.waitForSecondsThen(0.5f, jetPackOn, true));
        jetPackOn(true);
    }

    public override void OnPointerUp(PointerEventData eventData) { jetPackOn(false); }


    IEnumerator getPlayerThenStopUpdate() {
        while (!playerSet) {
            try {
                pl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
                playerSet = true;
            } catch (Exception e) { print("Exception " + e); }
            yield return new WaitForEndOfFrame();
        }

    }



}
