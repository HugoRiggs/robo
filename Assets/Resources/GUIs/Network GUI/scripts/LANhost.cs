﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class LANhost: EventTrigger {

    private void LaunchServer() {
        print("Server launching");
        Network.incomingPassword = "hardlyAPass";
        Network.InitializeServer(2, 7777, false);
    }

    public override void OnPointerClick(PointerEventData eventData) {
        LaunchServer();
        Destroy(gameObject);
    }

}