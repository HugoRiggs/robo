﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class LANclient: EventTrigger {

    private string ip = "";
    private int port = 7777;

    private void connect() {
        print("Connecting to " + ip + (":"+port));
        Network.Connect(ip, port);
    }

    public override void OnPointerClick(PointerEventData eventData) {
        connect();
        Destroy(gameObject);
    }

}