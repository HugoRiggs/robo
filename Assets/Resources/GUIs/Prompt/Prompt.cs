﻿using UnityEngine;
using System.Collections;

public class Prompt : MonoBehaviour {

    public string text;

    private float timeScale;

    void Start() {
        this.timeScale = Time.timeScale;
        Time.timeScale = 0.0f;
        GameObject.FindGameObjectWithTag("EZsplash").GetComponent<EZsplash>().displayText(text);
        TimeDependentFunctionCalling tdfc;
        StartCoroutine(tdfc.WaitForSecondsThen(5, afterSomeTime));
    }

    void afterSomeTime() {
        Time.timeScale = this.timeScale;
        GameObject.FindGameObjectWithTag("EZsplash").GetComponent<EZsplash>().displayText("");
        return;
    }


}
