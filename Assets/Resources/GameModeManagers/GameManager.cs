﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameManager : MonoBehaviour {

    public static GameManager current;
    public GameObject entryPortal;
    public GameObject player;
    public int difficultyModifier = 1;

    private SPAWN spawn;
    private delegate GameManager GetGameManager();
    private bool waiting;
    private float generalZaxis;

    TimeDependentFunctionCalling t;

    void Start() {
        spawn = FindObjectOfType<SPAWN>();

        // Portal player "enters" through
        StartCoroutine(t.DoThisThenWaitForSecondsThenDoThat(
            spawnEntryPortal, 0.75f, destroyEntryPortal));

        StartCoroutine(setPlayer());
        //player.transform.position = spawn.transform.position;
    }

    public IEnumerator setPlayer() {
        while (player == null) {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAttributes>().gameObject;
            yield return new WaitForSeconds(3f);
        }
        generalZaxis = player.transform.position.z;
    }

    void spawnEntryPortal() {
        entryPortal.transform.position = spawn.transform.position;
        entryPortal.SetActive(true);
    }

    void destroyEntryPortal() {
        entryPortal.SetActive(false);
    }

    public float getGeneralZaxis() {
        return generalZaxis;
    }

}
