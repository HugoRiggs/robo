﻿using UnityEngine;
using System.Collections;

/**
 * The class SurvivalGameMode is responsible for
 * spawning waves of enemies, and opening
 * a second section, and creating the next level portal.
 * 
 * Game object dependencies:
 *  - at least one enemy spawner object
 *  - a door like object which is deactivated after arbitrarily many waves.
 */ 

public class SurvivalGameMode : MonoBehaviour {

    public EnemySpawnerSP[] spawners;
    public EnemySpawnerSP[] spawnersNextArea;
	public GameObject player;
	public int blockageRemovedAtWave = 4;
	public int maxWaves = 4;
	public bool printDebug;
    public GameObject blockage, portalPrefab, portalSpawn;

    private bool waiting = true;
	private int numEnemiesThisWave = 0;
	private int overallNumEnemies = 0;
    private int waveNumber = 1;
    private int number_of_final_waves = 4;
	private bool completedLevel = true, inFinalWaves=false;
	private EZsplash spl;
    private TimeDependentFunctionCalling t;
    private int numEnemiesLeft;
    private int numEnemiesDestroyedThisRound = 0;
    private delegate GameManager GetGameManager();

    GetGameManager getGameManager = () => {
        return GameObject
        .FindGameObjectWithTag("GameManager")
        .GetComponent<GameManager>();
    };

    public IEnumerator waitOnPlayer() {
        print("waiting on player");
        while (player == null) {
            yield return new WaitForSeconds(3f);
            player = getGameManager().player;
        }
        waiting = false;
    }

    void Start () {
        spl = GameObject.FindGameObjectWithTag("EZsplash")
            .GetComponent<EZsplash>();
        StartCoroutine(waitOnPlayer());
        beginWave (waveNumber);
    }

	void FixedUpdate (){
        // While an instance of this object has detected the player object 
        if (!waiting) {

            // Count number of enemies remaning to be killed
            int enemiesRemaining = 0;
            foreach (EnemySpawnerSP spawner in spawners)
                enemiesRemaining += spawner.NumSpawneesStillLiving();

            // Test if we should spawn a new wave of enemies
            if (!completedLevel && enemiesRemaining == 0) {
                completedLevel = true; // Acknowledge completion of level
                int nextWaveNumber = waveNumber + 1; // increase wave counter

                print("Next wave  number =" + nextWaveNumber + "\nmax waves = " + maxWaves);

                // Test if we have not completed all the waves
                if (nextWaveNumber <= maxWaves) {
                    beginWave(nextWaveNumber); // begin a new wave

                    // test if the blockage should be removed
                    if(nextWaveNumber == blockageRemovedAtWave) { 
                    //if (!inFinalWaves) {
                        waiting = true;
                        inFinalWaves = true;
                        StartCoroutine(t.WaitForSecondsThen(4f, toggleWaiting));
                        spl.displayText("Secured Area");
                        StartCoroutine(t.WaitForSecondsThen(3f, spl.clearText));
                        spl.displayText("Opening lower section!");
                        StartCoroutine(t.WaitForSecondsThen(4f, spl.clearText));
                        GameObject.FindGameObjectWithTag("EZparticles")
                                .GetComponent<EZparticles>()
                                .explosion(blockage.transform.position, 2);
                        Destroy(blockage);
                        //maxWaves += 4;
                        spawners = spawnersNextArea;
                        completedLevel = false;
                    }
                } else {
                    spl.displayText("You survived!");
                    StartCoroutine(t.WaitForSecondsThen(3f, spl.clearText));
                    spl.displayText("Activating the portal");
                    StartCoroutine(t.WaitForSecondsThen(2f, spl.clearText));
                    portalPrefab.SetActive(true);
                    spl.displayText("Find the portal");
                    StartCoroutine(t.WaitForSecondsThen(4f, spl.clearText));
                }

                int playersHealth = player.GetComponent<PlayerAttributes>().getHealth();
                Debug ("player is dead " + (playersHealth <= 0) );
                if (playersHealth <= 0) {
                    Destroy (gameObject);
                }
            }
        }
    }

    private void toggleWaiting() {
        waiting = !waiting;
    }

	private void beginWave(int number) {
		waveNumber = number;
		spl.displayText ("Level " + number + " begining soon, prepare to fight!");
		StartCoroutine(t.WaitForSecondsThen(5f, spl.clearText));
		StartCoroutine(t.WaitForSecondsThen(10f, startSpawners));
	}


    private void startSpawners() {
        numEnemiesThisWave = 0;
        if (spawners.Length > 0) {
            foreach (EnemySpawnerSP spawn in spawners) {
                spawn.nEnemiesAtxSecondInterval(3, 3);
                numEnemiesThisWave += spawn.maxEnemyCount;
            }
            overallNumEnemies += numEnemiesThisWave;
        }
        completedLevel = false;
    }
		
	private void Debug(string[] args) {
		if (printDebug) {
			print ("DEBUG LOOP************");
			print ("KILLS = " + args [0] + " overall enemies = " + overallNumEnemies + ", wave " + waveNumber);
			print ("Enemies this round " + numEnemiesThisWave);
			print ("Next wave number = " + args [1]);
		}
	}

	private void Debug(string str) {
		if (printDebug) {
			print (str);
		}
	}

}
