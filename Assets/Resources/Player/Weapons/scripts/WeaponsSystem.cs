﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class WeaponsSystem : MonoBehaviour {

	public GameObject shellPrefab;
	public Transform shellSpawn;
	public GameObject shellInstance;
    public float fireRate = 0.4f;

    private bool reloading = false;

    // Commented out for GUI controlls
    /*
	void Update () {
		if (!isLocalPlayer)
			return;
		
		if ( Input.GetButton ("Fire") && !waiting) 
            fire();
		
	}*/

    public void fire() {
        if (!reloading) { 
            SPfire();
            reloading = true;
            StartCoroutine (waitFor ( fireRate ));
        }
    }

    private void SPfire() {
		var shell = (GameObject)Instantiate(shellPrefab, shellSpawn.position, shellSpawn.rotation);
		shell.GetComponent<Rigidbody> ().velocity = shell.transform.forward * 100 + gameObject.GetComponent<CharacterController>().velocity;
    }


	IEnumerator waitFor(float seconds){
		yield return new WaitForSeconds (seconds);
		reloading = false;
	}
}
