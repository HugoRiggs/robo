﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Melee : MonoBehaviour {

    public GameObject meleeObjectPrefab;
    public GameObject spawnFrom;
    public int fuelCost;
    public int damage;

    private bool canMelee = true;
    Animator anim;
    ClipManager clipManager;

    void Start() {
        anim = GetComponent<Animator>();
        clipManager = GameObject.FindObjectOfType<ClipManager>();
    }

    public void handleMelee() {
        Quaternion rot = Quaternion.LookRotation(-1 * transform.right + (transform.right * 2));

        // added a fuel consumption on melee use.
        int fuel = gameObject.GetComponentInParent<PlayerAttributes>().getFuel();
        if (canMelee && fuel > fuelCost) {
            StartCoroutine(meleeCoroutine((GameObject)Instantiate(meleeObjectPrefab, (spawnFrom.transform.position + transform.forward + (3.1f * transform.up)), rot)));
            gameObject.GetComponentInParent<PlayerAttributes>().setFuel(fuel - fuelCost);
            clipManager.PlaySound((int)clip.whip);
        }
    }


    IEnumerator meleeCoroutine(GameObject meleeObject) {
        meleeObject.transform.parent = spawnFrom.transform;
        canMelee = false;
        yield return new WaitForSeconds(0.33f);
        Destroy(meleeObject);
        yield return new WaitForSeconds(0.1f);
        canMelee = true;
    }
}
