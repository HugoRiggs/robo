﻿using UnityEngine;
using System.Collections;

public class particlesBurn : MonoBehaviour {

    void OnParticleCollision(GameObject other) {
	    if(other.gameObject.tag == "Enemy") {
		    other.GetComponent<EnemyController>().takeDamage(40);
	    }
    }
}
