﻿using UnityEngine;
using System.Collections;

public class shell : MonoBehaviour {

	public float travelForce = 800f;
	public float launchForce = 1500f;
	public float range = 100f;
	public int shellDamage = 25;
	public float explosionForce = 50f, explosionRadius = 15f;
	public ParticleSystem trailParticleFX;
	//public ParticleSystem explosionParticleFX; 

	private ClipManager clipManager;
	//private ParticleSystem explosionParticleFXInstance; 
	private Rigidbody rb;
	private Vector3 firedFrom;
	private bool exploded = false;

	void Awake(){
		firedFrom = transform.position;
		rb = GetComponent<Rigidbody> ();
		trailParticleFX = GetComponent<ParticleSystem> ();
        clipManager = GameObject.FindObjectOfType<ClipManager>();  //GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<ClipManager>();
        shellDamage = shellDamage * GameObject.FindObjectOfType<GameManager>().difficultyModifier;
	}

	void FixedUpdate(){
		if (Vector3.Distance (transform.position, firedFrom) > range)
			CmdExplosion();
		
		if (trailParticleFX.isStopped)
			Destroy (gameObject);
	}


	void OnCollisionEnter(Collision other){
        //StartCoroutine(knockBack (transform.position));
        if (other.transform.tag.Equals("Enemy")) {
            //print("Bam! hit " + other.transform.tag + ". dealt " + shellDamage + " dmg to " + other.transform.tag + "(now @ " + other.gameObject.GetComponent<EnemyController>().health + "hp)");
            EnemyController enemy = other.gameObject.GetComponent<EnemyController>();
            enemy.setHealth(enemy.getHealth() - shellDamage);
        }
        Destroy(gameObject);
		//Destroy (gameObject.GetComponent<MeshCollider>());
		//Destroy (gameObject.GetComponent<MeshRenderer>());
		trailParticleFX.Stop ();
		CmdExplosion();
	}


	//[Command]
	void CmdExplosion() {
		if(!exploded) {
			clipManager.PlaySound ((int)clip.explosion); 					// Audio effect.
			exploded = true;
			GameObject.FindGameObjectWithTag("EZparticles").
			GetComponent<EZparticles>().explosion(transform.position, 0);
			trailParticleFX.Stop();
			StartCoroutine(waitThenDestroyShell());
		}
	}

    IEnumerator waitThenDestroyShell() {
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    /*
	IEnumerator knockBack(Vector3 explosionPos){
		Collider[] colliders = Physics.OverlapSphere(explosionPos, explosionRadius);
		foreach(Collider hit in colliders) {
			Rigidbody rigb = hit.GetComponent<Rigidbody>();
			if(rigb!= null)
				rigb.AddExplosionForce(explosionForce, explosionPos, explosionRadius, 3.0f);
		}
		yield return new WaitForEndOfFrame();
	}
    */
}


