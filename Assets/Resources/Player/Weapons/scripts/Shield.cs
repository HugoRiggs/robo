﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

    public GameObject shieldObject;
    GameObject[,] shieldOs = new GameObject[4, 1];
    private int shieldCount = 0;
    private int shieldLastsSeconds = 30;
    private int cooldownBetweenPlacements = 10;
    private bool canPlaceShield = true;

    IEnumerator shieldCoroutine(Transform p) {
        if (canPlaceShield && shieldCount != shieldOs.Length) {
            GameObject.FindGameObjectWithTag("EZparticles").
            GetComponent<EZparticles>().explosion(p.position + (p.forward * 8) + (p.up * 2), 2);
            shieldOs[shieldCount, 0] = (GameObject)Instantiate(shieldObject, p.position + (p.forward * 8) + (p.up * 2), Quaternion.identity);
            StartCoroutine(scheduelForDistruction(shieldOs[shieldCount, 0], shieldLastsSeconds));
            canPlaceShield = false;
            shieldCount += 1;
        } else {
            yield return new WaitForSeconds(cooldownBetweenPlacements);
            canPlaceShield = true;
        }

    }

    IEnumerator scheduelForDistruction(GameObject x, int time) {
        yield return new WaitForSeconds(time);
        Destroy(x);
        shieldCount -= 1;
    }


    public void handleShield(Transform p) {
        StartCoroutine(shieldCoroutine(p));
    }


}
