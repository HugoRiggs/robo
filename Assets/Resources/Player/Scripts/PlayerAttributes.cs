﻿/**
 * Statistics and sound updates for the player.
 * This class handles player death. 
 * Also health, stamina, damage system, kills, fuel.
 * 
 * This class works and is somewhat tightly coupled with player movement.
 */

 // TODO: loosly couple this PlayerAttributes with PlayerMovement.
 // fewer interdependence will make small edits to the code easier.

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

[System.Serializable]
public class PlayerAttributes : MonoBehaviour {

	// Game objects
	public Slider hpSlider; //onscreen hp bar
	public Slider energySlider; //onscreen hp bar
	public Text scoreText;
    public GameObject HeadsUpDisplay;
    public bool unlimitedHealth = false;
    public bool fuelRecharges = false;

	public int kills = 0;
	public static bool hasJetpack = false;

	private GameObject HUD;
	private ClipManager clipManager; 
	private bool hasFlag = false;
	private static int currentHP = 0;
	private static int maxHP = 100;
	private static int maxFuel = 200;
	public  int fuel = 200; // change back to private
	private static int score;
	private bool damaged;
    private bool debugPrint = false;//true;
    private bool doRecharge = true;
	private PlayerMovement playerMovement; //movement script, to be disabled upon death

	// Use this for initialization
	void Awake() {
		currentHP = maxHP;
		HUD = Instantiate(HeadsUpDisplay);
		hpSlider = GameObject.Find("HealthSlider").GetComponent<Slider>();
		energySlider = GameObject.Find("EnergySlider").GetComponent<Slider>();
		playerMovement = GetComponent<PlayerMovement>();
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        clipManager = GameObject.FindObjectOfType<ClipManager>();//gameObject.GetComponentInChildren<ClipManager>();
        clipManager.gameObject.SetActive(true);
	}

	void FixedUpdate() {
		if (currentHP <= 0) { OnDeath(); }
		if (GetComponent<Jumpboots>().jumpFX[0].activeInHierarchy) { clipManager.jetPack(); }
        else { clipManager.stopJetPack(); }

        // If we are moving and not in the air play the footstep clipManager
        CharacterController cc = playerMovement.GetComponent<CharacterController>();
        if ((cc.velocity.x > 1 || cc.velocity.x < -1))
            if (cc.isGrounded)
                clipManager.footSteps();

        if(doRecharge && fuelRecharges && cc.isGrounded)
            StartCoroutine(rechargeFuel());
	}

    IEnumerator rechargeFuel() {
        InputControlStruct ios = FindObjectOfType<InputControl>().io;
        doRecharge = false;
        if(ios.isCrouching())
            setFuel(getFuel() + 3);
        else
            setFuel(getFuel() + 1);
        yield return new WaitForSeconds(2*GetComponent<Jumpboots>().burnFuelCooldown);
        doRecharge = true;
    }


	void OnCollisionEnter(Collision collide) {
		string tag = collide.gameObject.tag;

		if (tag == "Terrain") { clipManager.stopJetPack(); }

		if (tag == "Jetpack") {
		    hasJetpack = true;
		    fuel = 100;
		    Destroy(collide.gameObject);
		}

		if (tag == "Gas Collectible") {
		    if (hasJetpack == true) {
                fuel = fuel + 10; //FuelPickup.FuelVal Destroy(collide.gameObject);
                if (fuel > 1000) {
                    fuel = 1000;
                }
		    }
		}

        /// Stuff that will cause pain
        if (tag == "Enemy") {
            debug("enemy hit us");
            takeDamage((int)collide.gameObject.GetComponent<EnemyController>().getDamage());
            setScore(getScore() - 2);
        } else if (tag == "enemyShell") {
            takeDamage((int)collide.gameObject.GetComponent<shell>().shellDamage);
            setScore(getScore() - 2);
        }

		if(tag == "Hazard")
		{
		    //check if the object is an instant death, if not apply correct damage
		    if (collide.gameObject.GetComponent<HazardController>().instaDeath == false)
		    {
			Debug.Log(collide.gameObject.name 
                + " hit the player for " + collide.gameObject.GetComponent<HazardController>().damage + " damage");
			takeDamage((int)collide.gameObject.GetComponent<HazardController>().damage);
		    }
		    else
			OnDeath();

		}
		///Stuff that will hurt you ends here

		if (tag == "ScoreItem") {
			Debug.Log(" played picked up a " + collide.gameObject.name);
			setScore (getScore() + 5);
		}
	}


    public void takeDamage(int amt) {
        if (!damaged && !unlimitedHealth) {
            damaged = true;
            currentHP -= amt;
            hpSlider.value = currentHP;
            clipManager.PlaySound((int)clip.collisionHit);
            StartCoroutine(Flash());
            StartCoroutine(dmgWait());
        }
    }

	public void setHealth(int amt) {
        if (currentHP + amt <= maxHP) {
            currentHP += amt;
        } else
            currentHP = maxHP;

        hpSlider.value = currentHP;
	}

	// Coroutine to provide player invincibility for a while after being hit.
	IEnumerator dmgWait() {
		yield return new WaitForSeconds(0.5f);
		damaged = false;
	}

	// blinking player mech 3 times when taking damage
	IEnumerator Flash() {
		for (var i = 0; i < 3; i++) {
		    //'remove' player mech
		    GameObject.Find("Circle").GetComponent<Renderer>().enabled = false;
		    //wait 500 ms
		    yield return new WaitForSeconds(0.08f); 
		    // 'add' player mech
		    GameObject.Find("Circle").GetComponent<Renderer>().enabled = true;
		    yield return new WaitForSeconds(0.08f);
		}
	}

	public void OnDeath() {
		currentHP = 0;
		hpSlider.value = currentHP;
		Time.timeScale = 0;
		playerMovement.enabled = false;
		SceneManager.LoadScene("98 Death Screen");
	}

	public int getHealth() { return currentHP; }

	public int getFuel() { return fuel; }

	public void setFuel(int amount){
        if (amount < maxFuel)
            fuel = amount;
        else
            fuel = maxFuel;
        energySlider.value = fuel;
    }

	public int getScore() { return score; }

	public void setScore(int x){
		score = x;
		scoreText.text = "Score: " + score.ToString();
	}

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Coin") {
            Destroy(other.gameObject);
            setScore(getScore() + 1);
            clipManager.PlaySound((int)clip.coinGrab);
        }

        if (other.CompareTag("CombatZone")) {
        }

        if (other.transform.tag == "healthGoodie") {
            setHealth(getHealth() + 20);
            Destroy(other.gameObject);
        }
        if (other.transform.tag == "fuelGoodie") {
            setFuel(getFuel() + 20);
            Destroy(other.gameObject);
        }
    }


    public bool playJetBootsClipFunc() {
        clipManager.PlaySound((int)clip.jetBoots);
        return false; // disables replay
    }


    void OnControllerColliderHit(ControllerColliderHit other) {
        if (other.transform.tag == "healthGoodie") {
            setHealth(getHealth() + 20);
            Destroy(other.gameObject);
        }
        if (other.transform.tag == "fuelGoodie") {
            setFuel(getFuel() + 20);
            Destroy(other.gameObject);
        }

    }


    void debug(string str) {
		if (debugPrint)
		    print(str);
	}
}