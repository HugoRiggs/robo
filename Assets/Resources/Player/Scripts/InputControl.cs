﻿using System;
using UnityEngine;

/**
 * Functions return true if the input is occuring. 
 * An easy way to test for input in the player movement code.
 */


public class InputControl : MonoBehaviour {
    public InputControlStruct io;
    void Start() {
        io = new InputControlStruct();
    }
}

public struct InputControlStruct {

//    public bool MagnetBootsInput() {
//        return Input.GetKey(KeyCode.O);
//    }

    public bool InteractInput() {
       return Input.GetButtonDown("Interact");
    }

    public bool JumpInput() {
        return Input.GetButton("Jump");
        //return Input.GetButtonDown("Jump");
    }

    public bool JetpackInput() {
        return Input.GetButton("Jump");
    }

    public bool glideInput() {
        return Input.GetKey(KeyCode.W);
    }

    public bool FireInput() {
        return Input.GetKey(KeyCode.J);
    }

    public bool isStrafing() {
        return Input.GetKey(KeyCode.L);
    }

    public bool isCrouching() {
        return Input.GetKey(KeyCode.LeftControl);
    }

    public bool isSprinting() {
        return Input.GetKey(KeyCode.LeftShift);
    }

    public float horizontalInput() {
        return Input.GetAxis("Horizontal"); 
    }

    public bool isMeleeing() {
        return Input.GetButtonDown("Melee");
    }

    public bool downJetsInput() {
        return Input.GetKey(KeyCode.S);
    }

    public bool isShielding() {
        return Input.GetKey(KeyCode.L);
    }

    public bool overclock() {
        return Input.GetKey(KeyCode.O);
    }

}
