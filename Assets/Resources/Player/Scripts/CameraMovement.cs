﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CameraMovement : MonoBehaviour {
	public float smoothTimeY = 0.5f; // If you want to make changes to the camera movement follow rate
	public float smoothTimeX = 0.5f; // make these public and then they can be accessed through the inspector.

	private Vector2 velocity;
	private float xTmp;
	private float yTmp;
	private static float horzShift, vertShift;
	private float vertShTmp, horzShftTmp;
	private GameObject player;
	private CharacterController playersCC;
    private bool shiftsEnabled; // Shift camera as player moves 

	void awake () {
		xTmp = smoothTimeX;
		yTmp = smoothTimeY;
		horzShift = 0;
		vertShift = 6;
		vertShTmp = vertShift;
		horzShftTmp = horzShift;
		shiftsEnabled = true;
	}

	
	void FixedUpdate () {
		try{
            player = FindObjectOfType<GameManager>().player; // todo: replace this line
            leadPan();

			float posX = Mathf.SmoothDamp(
					transform.position.x,
					player.transform.position.x+horzShift,
					ref velocity.x, smoothTimeX);
			float posY = Mathf.SmoothDamp(
					transform.position.y,
				       	player.transform.position.y+vertShift,
				       	ref velocity.y, smoothTimeY);
			transform.position = new Vector3(posX, posY, transform.position.z);
		}catch {  }
	}


	private void setShiftsEnabled(bool b){ shiftsEnabled = b; }


	private void leadPan(){
		if (shiftsEnabled) {
		    if (playersCC.velocity.x > 10)
			horzShift = 20;
		    else if (playersCC.velocity.x < -10)
			horzShift = -20;
		    else
			horzShift = horzShftTmp;

		    if (playersCC.velocity.y > 10)
			vertShift = 5;
		    else if (playersCC.velocity.y < -10)
			vertShift = -5;
		    else
			vertShift = vertShTmp;
		} else {
		    horzShift = 1;
		    vertShift = 1;
		}
	}

}