﻿/**
*   Implementation with Movement Controller specific for 
*   the player control.
*/

using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerMovement : MonoBehaviour {

    private const float maxSqrMagnitude = 3200;

    public float flightForce = 40;
    public float jumpForce = 40;
    public float glideForce = 40;
    public bool flightEnabled = true;

    private float speedCoef = 1;

    private float horizontalForceMod = 1f;
    public float walkSpeed = 15;
    public float sprintSpeed = 20;
    private float inputH = 0;
    // Change sideways speed variables
    private float horizontalForceModMax = 1.3f;
    private float horizontalForceModDefault = 1;
    // Measure side ways up down speed variables
    private float velocityX;
    private float velocityY;
    private float Zvalue;    // A place to hold centered z value
    private bool waiting = false;
    private bool debugPrint = false;
    private InputControlStruct io;
    private  GameObject cameraPrefab;
    private Vector3 right = new Vector3(1f, 0f, 0f);
    private Vector3 left = new Vector3(-1f, 0f, 0f);
    private GameObject ourCam;
	private PlayerAttributes attributes;
	private Animator animator;
	private WeaponsSystem weaponsSystem;
    private GravityAttractor gravAttractor;
    private GameObject[] gravAttractors;
    private Vector3 smoothMoveVelocity, spawnPosition, moveDirection, spawn;
    private Jumpboots jumpboots;
    private MovementController movementController;

    // custom debug function
    private void debug(string str) {
        if (debugPrint)
            Debug.Log(str);
    }

    private bool jumpInput() { return io.JumpInput(); }
    private bool interactInput() { return io.InteractInput(); }
    private bool meleeInput() { return io.isMeleeing(); }
    private float horizontalInput() { return io.horizontalInput(); }

    // BUILT IN UNITY FUNCTIONS
	void Awake() {
		try { weaponsSystem = gameObject.GetComponent<WeaponsSystem>();	// Load weapons system
		} catch { debug("no weapons system"); }
        // Set default gameplay values
        transform.forward = right;
        attributes = GetComponent<PlayerAttributes>();
        animator = GetComponent<Animator>();            // put animator in variable
        jumpboots = GetComponent<Jumpboots>(); // put jets in variable
        jumpboots.CmdjetsOff();         // turn off jets
        Respawn();                      // firstly spawn player
        io = FindObjectOfType<InputControl>().io; // A struct which abstracts input controlls
        attributes = gameObject.GetComponent<PlayerAttributes>();
        movementController = GetComponent<MovementController>();
        movementController.setZvalue(transform.position.z);
	}

    void Update() {
        Debug.DrawRay(transform.position, transform.up * -1);
        if (movementController.isGrounded())
            animator.SetFloat("Speed", Mathf.Abs(velocityX));//+velocityY
        else
            animator.SetFloat("Speed", 0);
        animator.SetBool("Jumping", !movementController.isGrounded());
        animator.SetBool("Sprinting", io.isSprinting());
        animator.SetBool("Crouching", io.isCrouching());
        animator.SetBool("meleeing", meleeInput());
        animator.SetInteger("Health", attributes.getHealth());
    }


    void FixedUpdate() {
        velocityX = movementController.velocityX();
        velocityY = movementController.velocityY();
        bool isGrounded = movementController.isGrounded();
        movementController.resetMovement();
        if (movementController.isJumping()) 
            movementController.moveUp(jumpForce);
        if (interactInput()) { animator.CrossFade("Interact", .5f); } // interact with switches
        if (meleeInput())        // Melee
            GetComponent<Melee>().handleMelee();

        // added a fuel consumption on jumping
        if (jumpInput() && isGrounded) {     // Jump
            int fuel = gameObject.GetComponent<PlayerAttributes>().getFuel();
            if(fuel - 5 > 0) {
                movementController.jump();
                gameObject.GetComponent<PlayerAttributes>().setFuel(fuel - 5);
            }
        }
        if (!io.isCrouching())   // Left/Right movement
            if (!meleeInput()) {
                handleLRmovement();
            } else if (isGrounded) {
                inputH = velocityX - 1;
            }
        if (flightEnabled) {
            if (!movementController.isJumping() && io.JetpackInput() && attributes.getFuel() > 0) { // jetpack Condition OR jump
                horizontalForceMod = horizontalForceModMax * speedCoef;
                movementController.moveUp(flightForce * speedCoef);
                jumpboots.activate();
            } else if (!isGrounded && io.glideInput() && !io.JetpackInput() && attributes.getFuel() > 0) {  // Glide condition
                horizontalForceMod = horizontalForceModMax * speedCoef;
                movementController.moveUp(glideForce);
                jumpboots.coast();
            } else if (io.downJetsInput() && !isGrounded && attributes.getFuel() > 0) { // Downwards jetpack
                movementController.moveDown(jumpForce * speedCoef);
                jumpboots.downJets();
            } else {        // not jumping or using jetpack
                horizontalForceMod = horizontalForceModDefault;
                jumpboots.CmdjetsOff();
                jumpboots.downwardJetEffectsOff();
            }
        }
        if (io.FireInput()) // Use projectile weapon conditional check
            weaponsSystem.fire();
        if (!isGrounded &&  io.JetpackInput()) // Sideways momentum increases when flying with jetpack
            increaseHorzMod();
        else
            decreaseHorzMod();
        movementController.applyGravity();
        movementController.applyMovement();
    }

    // CUSTOM MEMBER FUNCTIONS

    private void handleLRmovement() 
    {
        // TODO uncomment for pubish
        // Commented out to allow GUI control input
        #if UNITY_STANDALONE || UNITY_WEBPLAYER
            inputH = horizontalInput(); // Left and Right movement input
        #endif
        Vector3 forwards = // Determine direction of movement by positive or negative input value, and strafe button
            (inputH < 0) && !io.isStrafing() 
            ? -transform.forward * inputH 
            : transform.forward * inputH;
        float moveForce = io.isSprinting() ?   sprintSpeed :  walkSpeed; // When holding shift key, toggle sprinting\walking
        moveForce *= horizontalForceMod;
        // Set direction facing
        if (inputH != 0 && !io.isStrafing()) {
            if (inputH > 0) {
                setDirectionFacing(right);
                movementController.moveRight(moveForce);
            }
            if (inputH < 0) {
                setDirectionFacing(left);
                movementController.moveLeft(moveForce);
            }
        }
        RaycastHit wallHitInfo;
        if (Physics.Raycast(
            new Vector3(transform.position.x,
                transform.position.y + 3.5f,
                transform.position.z + 2f),
                transform.forward,
                out wallHitInfo, 6.3f)
                ) { // If we run into a wall,
                    if (wallHitInfo.transform.tag == "Terrain") {
                        inputH = 0; // do not continue moving forward.
                    }
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit) { // Enemy hits are handled in enemy controller

    }

    public void Respawn() {
		try {
			if (GameObject.FindGameObjectWithTag("Respawn").transform.position != null) 
				transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
		 } catch { debug("no spawn object "); }
	}

	private void Respawn(Vector3 pos) {
		transform.position = pos;
	}

    private void resetHorizontalForceCoefficient() {
        horizontalForceMod = horizontalForceModDefault;
    }

    private void increaseHorzMod() {
        if (horizontalForceMod < horizontalForceModMax) {
            horizontalForceMod += 0.1f;
        }
    }

    private void decreaseHorzMod() {
        if (horizontalForceMod > horizontalForceModDefault)
            horizontalForceMod -= 0.1f;
    }

   void setDirectionFacing(Vector3 direction) { transform.forward = direction; }

    bool facingLeft() {
        if (transform.forward == right)
            return false;
        else
            return true;
    }

    float abs(float x) { return Mathf.Abs(x); }
    float distance(float point1, float point2) { return abs(point1 - point2); }

    // use with buttons on screen
    public void moveRightConditional(bool b) {
        if (b) {
            print("mov right");
            inputH = 1;
        } else {
            print("stop mov right");
            inputH = 0;
        }
    }

    void moveLeftConditional(bool b) {
        if (b)
            inputH = -1;
        else
            inputH = 0;
    }

}