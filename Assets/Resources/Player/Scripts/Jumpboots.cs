﻿using UnityEngine;
using System.Collections;

public class Jumpboots : MonoBehaviour {

    public GameObject[] jumpFX;
    public bool unlimitedFuel = false;
    public float upForce = 10;
    public float downForce = 10;
    public float burnFuelCooldown = 1f;

    private InputControlStruct i_o;
    private bool canBurnFuel = true;
    private bool jetBootsEnabled = true;


    public void Start() {
        i_o = FindObjectOfType<InputControl>().io;
        downwardJetEffectsOff();
           CmdjetsOff();
    }

    public void activate() {
        downwardJetEffectsOff();
        if (canBurnFuel)
            StartCoroutine(burnFuel(2));
        CmdjetsOn();
    }

    public void  downJets() {
           CmdjetsOff();
            if (canBurnFuel)
                StartCoroutine(burnFuel(2));
            downwardJetEffectsOn();
    }

    public bool jetsAreOn() { return jumpFX[0].activeInHierarchy; }

    public IEnumerator burnFuel(int n) {
        if (!unlimitedFuel) {
            canBurnFuel = false;
            GetComponent<PlayerAttributes>().setFuel(GetComponent<PlayerAttributes>().getFuel() - n);
            yield return new WaitForSeconds(burnFuelCooldown);
            canBurnFuel = true;
        }
    }

    void OnTriggerEnter(Collider other) {
        if(other.tag == "unlimitedFuelField") {
            unlimitedFuel = true;
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.tag == "unlimitedFuelField") {
            unlimitedFuel = false;
        }
    }


    public void coast() {
        downwardJetEffectsOff();
        CmdjetsOn();
        if (canBurnFuel) 
            StartCoroutine(burnFuel(1));
    }
    
   /**
	* Jet boots functionality is aided by these members.
	* - Some of these are just toggles.
	*/	
	public IEnumerator jetBootsCoolDown(){
		yield return new WaitForSeconds (0.7f);
		if(jumpFX[1].activeInHierarchy)
			toggleJetBoots ();
	}

    public void toggleJetBoots() {
        jetBootsEnabled = jetBootsEnabled ? false : true;
    }

    public void downwardJetEffectsOn() { // disable/enable effect on client
        jumpFX[2].SetActive(true);
        jumpFX[3].SetActive(true);
    }

    public void downwardJetEffectsOff() { // disable/enable effect on client
        jumpFX[2].SetActive(false);
        jumpFX[3].SetActive(false);
    }

	public void CmdjetsOn(){ // disable/enable effect on client
		jumpFX[0].SetActive(true);
		jumpFX[1].SetActive(true);
	}

	public void CmdjetsOff(){
		if( ! i_o.isMeleeing() )
			jumpFX[0].SetActive(false);
		jumpFX[1].SetActive(false);
    }

	public IEnumerator WaitForJump() {  //function that is used to deactivate jumpFX after a time interval
		yield return new WaitForSeconds(0.3f);
		CmdjetsOff ();
	}

}
