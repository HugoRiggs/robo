﻿using UnityEngine;
using System.Collections;

public class CollectablesTrigger : MonoBehaviour {

    PlayerAttributes playerAttributes;


    void Start() {
        playerAttributes = gameObject.GetComponentInParent<PlayerAttributes>();


    }

    void OnTriggerEnter(Collider other) {
        if (other.transform.tag == "healthGoodie") {
            playerAttributes.setHealth(playerAttributes.getHealth() + 20);
            Destroy(other.gameObject);
        }
        else if (other.transform.tag == "fuelGoodie") {
            playerAttributes.setFuel(playerAttributes.getFuel() + 20);
            Destroy(other.gameObject);
        }
    }

}
