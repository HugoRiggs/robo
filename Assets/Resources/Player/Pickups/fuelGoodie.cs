﻿using UnityEngine;
using System.Collections;

public class fuelGoodie : MonoBehaviour {

    public float lifeInSeconds;
    public float force;

    void FixedUpdate() {
        GameObject plyr = GameObject.FindGameObjectWithTag("Player");
        Rigidbody rb = GetComponent<Rigidbody>();
        Vector3 plyrPos = plyr.transform.position;

        //transform.rotation = plyr.transform.rotation;//Quaternion.RotateTowards(transform.rotation, plyr.transform.rotation, 180);//Quaternion.LookRotation(plyr.transform.forward, plyr.transform.up);

        transform.rotation = Quaternion.identity;
        if (plyrPos.x > transform.position.x)
            rb.AddForce(rb.transform.right * force * Time.deltaTime);
        else
            rb.AddForce(-1 * rb.transform.right * force * Time.deltaTime);

        if (plyrPos.y > transform.position.y)
            rb.AddForce(rb.transform.up * force * Time.deltaTime);
        else
            rb.AddForce(-1 * rb.transform.up * force * Time.deltaTime);

        if (plyrPos.z > transform.position.z)
            rb.AddForce(rb.transform.forward * force * Time.deltaTime);
        else
            rb.AddForce(-1 * rb.transform.forward * force * Time.deltaTime);
    }

    void Start() {
        StartCoroutine(lifetime());
    }

    IEnumerator lifetime() {
        yield return new WaitForSeconds(lifeInSeconds);
        Destroy(gameObject);
    }
}
