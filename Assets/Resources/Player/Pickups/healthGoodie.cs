﻿using UnityEngine;
using System.Collections;

public class healthGoodie : MonoBehaviour {
    public float lifeInSeconds;
    public float force;

	void FixedUpdate () {
        Rigidbody rb = GetComponent<Rigidbody>();
        GameObject plyr = GameObject.FindGameObjectWithTag("Player");
        Vector3 plyrPos = plyr.transform.position;

        transform.rotation = Quaternion.identity;
        if (plyrPos.x > transform.position.x)
            rb.AddForce(rb.transform.right * force * Time.deltaTime);
        else
            rb.AddForce(-1 * rb.transform.right * force * Time.deltaTime);

        if (plyrPos.y > transform.position.y)
            rb.AddForce(rb.transform.up * force * Time.deltaTime);
        else
            rb.AddForce(-1 * rb.transform.up * force * Time.deltaTime);

        if (plyrPos.z > transform.position.z)
            rb.AddForce(rb.transform.forward * force * Time.deltaTime);
        else
            rb.AddForce(-1 * rb.transform.forward * force * Time.deltaTime);
    }

    void Start() { StartCoroutine(lifetime()); }


    IEnumerator lifetime() {
        yield return new WaitForSeconds(lifeInSeconds);
        Destroy(gameObject);
    }
}
