﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class EnemySpawner : NetworkBehaviour {

	public GameObject EnemyPrefab;
	public float spawnIntervalSeconds;

	private Transform spawnTransform;
	private TimeDependentFunctionCalling t;
	private int maxEnemyCount = 10, counter=0;

	void Start () {
		print ("Enemy spawner  in the map");
		spawnTransform = gameObject.transform;

		StartCoroutine(enemySpawning (counter));
	}




	IEnumerator enemySpawning(int counter){
		yield return new WaitForSeconds (spawnIntervalSeconds);
		if (counter < maxEnemyCount) {
			spawnEnemyIntervalometer ();
			StartCoroutine(enemySpawning (counter + 1));
		} 
	}

	void spawnEnemyIntervalometer(){
		CmdspawnEnemy ();
	}

		
	[Command]
	private void CmdspawnEnemy(){
		Instantiate (EnemyPrefab, spawnTransform.position, spawnTransform.rotation);
	}
}
