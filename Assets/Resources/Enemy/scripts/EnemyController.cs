﻿/**
*   Class Enemy controller,
*   This script will control enemies behavior.
*   Tell's an enemy it to move, 
*   call damage functions when touched,
*   or start an idle state.
*/
using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour 
{
    public GameObject healthGoodie;
    public GameObject fuelGoodie;
    public float speed = 50;      // our speed
    public const float horizontalForceMod = 1;    // Our side moving speed modifier
    public float knockBackForce;
    public float upForce = 150;
    public float distToGround = 1;
    public int damage = 10;     // damage we inflict @difficulty_modifier
    public int health = 50;   // our health @difficulty_modifier

    public float fallingRateUnit = 1f;
    public float fallingRateDefault = 1f;
    public float fallingRateMax = 60;    // max fallingRate can go to
    public float fallingRate = 1;


    private float zValue;
    private float distToCeil = 2;   private float moveHorizontal = -1; //1 is backwards at start

    private const float sqrMaxSpeed = 625;    // Max speed squared
    private  Vector3 right = new Vector3(90, 0, 0);
    private Vector3 left = new Vector3(90, 0, 0) * -1;

    private int flightCooldownTime = 3; // @difficulty_modifier 

    public bool isGrounded = false;
    private bool alive = true;
    private bool debugPrint = false;
    private bool exploded = false;
    private bool flightCooledDown = true;
    private bool canPatrol = true;
    private bool ranIntoWall = false;
    private bool canChangeDirOnWallHit = true;
    private bool shouldJump = false;
    private bool acrewFallingRate_b = false;

    private string idleAnimName = "Idle";
    private string walkAnimName = "Walk_Cycle";

    private GameObject player = null;   // An object to reference player position
    private Animator animator = null;
    private Patrol patrol;
    private TimeDependentFunctionCalling t;
    private Vector3 movement;
    private Rigidbody rb;


    // INITIALIZATION ASSISTANT MEMBER FUNCTION
    IEnumerator waitOnPlayer() {
        while (player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForSeconds(2f);
        }
    } // END

    //  UNITY ENGINE BUILT IN VOID MEMBER FUNCTIONS 
    void Start() {
        rb = GetComponent<Rigidbody>(); 
        zValue = transform.position.z;
        StartCoroutine(waitOnPlayer());     // wait for player
        patrol.generateNewValues();        // Initialization for patrol mode,
        patrol.waiting = false;            // and allow starting.
        damage = damage * GameObject.FindObjectOfType<GameManager>().difficultyModifier;
    }

    void FixedUpdate() {
        print("distance to ground " + distToGround);
        transform.position = new Vector3(transform.position.x, transform.position.y, zValue);
        isGrounded = IsGrounded();
        movement = new Vector3(0, 0, 0);
        acrewFallingRate_b = true;

        float velocityX = rb.velocity.x;
        float velocityY = rb.velocity.y;

        if (player != null) { /// If player detected
            switch (gameObject.name.Substring(0, 7)) { /// Handle our AI 
                case "EnemyMe":
                    animator = gameObject.GetComponent<Animator>();
                    doChase();
                    if (Mathf.Abs(velocityX) > 0.5)
                        animator.SetBool("walking", true);
                    else
                        animator.SetBool("walking", false);
                    break;
                case "floater":
                    doChase();
                    break;
            }
            checkHealth();
            if (shouldJump && alive) {
                movement.y += upForce * 25; 
                shouldJump = false;
            }
            if (!acrewFallingRate_b)
                fallingRate = fallingRateDefault;
            applyGravity();
            rb.AddForce(movement);
        }
    }

    void OnParticleCollision(GameObject other) {
        if (other.tag == "Fire") {
            print("taking fire damage");
            takeDamage(1);
        }
    }

    void OnControllerColliderHit(ControllerColliderHit col) {
//        if (col.gameObject.tag == "Player") {       // Collided with player
//            debug(this.gameObject.name + " hit the player for " + damage + " damage");  // log.debug 
//            col.gameObject.GetComponent<PlayerAttributes>().takeDamage(damage); // tell player to take damage
//            col.gameObject.GetComponent<CharacterController>().Move(transform.forward * knockBackForce);  // player gets knocked back
//            knockBackSelf();
//        }
//        if (col.gameObject.tag == "Enemy") { // if we hit enemy
//            knockBackSelf();
//        }

    }

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.tag == "Shell") { // If a shell hits us,
            this.takeDamage(40);               //  we get damaged.
        }
    }

    void OnTriggerEnter(Collider TrigCollide) { //  if you hit an edge trigger turn around
        if (TrigCollide.gameObject.tag == "Edge") {
            moveHorizontal = moveHorizontal * -1;
        }
        if (TrigCollide.gameObject.tag == "Melee") {
            takeDamage(
                GameObject.FindObjectOfType<Melee>().damage 
            );
        }
    } //  END UNITY BUILT IN VOID MEMBER FUNCTIONS //

    // OUR CUSTOM IMPLEMENTATION MEMBER FUNCTIONS //
    public void destroyEnemyObject() {      // used to destroy instance object of enemy controller
        GameObject.FindGameObjectWithTag("EZparticles").
        GetComponent<EZparticles>().explosion(transform.position, 1);
        Destroy(gameObject);
    }

    void checkHealth() {
        if ((areFloatsApproxEqual(health, 0.0f) || health < 0.0f) && alive) { /// Check if the enemy is dead
            alive = false;
            CmdExplosion();
            if(gameObject.name == "EnemyMechAFBX")
                animator.SetBool("Dead", true);
            StartCoroutine(t.WaitForSecondsThen(0.4f, dropGoodies));
            StartCoroutine(t.WaitForSecondsThen(0.5f, destroyEnemyObject));
            PlayerAttributes pa = player.GetComponent<PlayerAttributes>();
            pa.setScore(pa.getScore() + 17);
            pa.kills += 1;
        }
    }

    private void doChase() 
    {
        float movementForce = speed * horizontalForceMod;
        ChasePlayerAI chaseAI = GetComponent<ChasePlayerAI>();

        // only move LR if grounded
        if (!isGrounded && !gameObject.name.Contains("floater")) {
            movementForce = speed / 10;
            movement.x = 0;
        }

        if (chaseAI.playerOnRight && chaseAI.seesPlayer) {  // move right logic
            transform.forward = right;
            movement.x += movementForce;
        } else if (chaseAI.seesPlayer) {            // move left logic
            transform.forward = left;
            movement.x -= movementForce;
        } else {
            if (canPatrol)
                doPatrol();
        }

        // Move up depending on relative location of player
        if (chaseAI.playerAbove && chaseAI.verticalChasingEnabled && flightCooledDown) {
            acrewFallingRate_b = true;
            movement.y += upForce;
        } else 
        { StartCoroutine(flightCooldown()); }
    }

    private IEnumerator flightCooldown() { flightCooledDown = false; yield return new WaitForSeconds(flightCooldownTime); flightCooledDown = true; }

    private bool IsGrounded() {
        Debug.DrawRay(transform.position, -Vector3.up);
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    private bool IsTouchingCeiling() {
        return Physics.Raycast(transform.position, Vector3.up, distToCeil + 0.1f);
    }


    bool cooledDown = true;
    public void moveAroundCeiling() {
        //print("moving around ceiling");
        //transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 10);
        if(cooledDown)
            StartCoroutine(toggleCapsuleCollider());
    }

    IEnumerator toggleCapsuleCollider() {
        cooledDown = false;
        gameObject.GetComponent<CapsuleCollider>().isTrigger = true;
        yield return new WaitForSeconds(0.5f);
        cooledDown = true;
        gameObject.GetComponent<CapsuleCollider>().isTrigger = false;

    }

    public void jump() { shouldJump = true; }

    private void dropGoodies() {        // a function to spawn health and fuel pickups
        System.Random rnd = new System.Random();             // 2 random int, rolls
        int numHealthGoodies = rnd.Next(1, 4);               // between 1 and 4
        int numFuelGoodies = rnd.Next(1, 4);                 // 
        for (int i = 0; i < numHealthGoodies; i++) {                            // Iteritively create goodie objects
            Instantiate(healthGoodie, transform.position, transform.rotation);  //
        }                                                                       //
        for (int i = 0; i < numHealthGoodies; i++) {                            //
            Instantiate(fuelGoodie, transform.position, transform.rotation);    //
        }                                                                       //
    }

    bool areFloatsApproxEqual(float a, float b) { return Mathf.Abs(a - b) < 0.01; }    // Approximate to one one-hundredth

    void knockBackSelf() {
        if (transform.forward == left)
            movement.x += knockBackForce;
        else
            movement.x -= knockBackForce;
    }

    public void takeDamage(int amount) { health-=amount; }  // Apply damage to us
    public int getDamage() { return damage; }
    public int getHealth() { return health; }
    public void setHealth(int amount) { health = amount; }

    struct Patrol {   // Represents patrol behavior
        public bool waitForTimeOutInPatrol;
        public bool waiting;
        public int walkForThisManySecondsRandom;
        public int dir;
        public int walkForThisManySeconds;
        public void generateNewValues() {
            waitForTimeOutInPatrol = true;
            walkForThisManySeconds = Random.Range(1, 5);
            dir = Random.Range(-1, 2);
        }
    }

    public void applyGravity() {
        float gravityForce = GameObject.FindGameObjectWithTag("EZgravity").GetComponent<EZgravity>().getForce();
        if (!IsGrounded() && fallingRate < fallingRateMax)
            fallingRate += fallingRateUnit;
        if (IsGrounded())
            fallingRate = fallingRateDefault;
        movement.y -= gravityForce * Mathf.Pow(fallingRate, 2);
    }

	private bool flipBool(bool x) { if (x){ x = false;  return x;} else{ x = true; return x;} }

    private void resetTimeout() { patrol.waitForTimeOutInPatrol = false; }

	private void doPatrol() {
		debug("***********************");
		debug("Patrolling\ndir " + patrol.dir);
		debug("walkForThisManySeconds " + patrol.walkForThisManySeconds);
		debug("waitForTimeOutInPatrol " + patrol.waitForTimeOutInPatrol);
		debug("waiting " + patrol.waiting);
        debug("ran into something " + ranIntoWall);

		ranIntoWall = false;
		if(patrol.waitForTimeOutInPatrol && !patrol.waiting){
			debug("about to wait for esconds then rset timeout ");
			StartCoroutine ( t.WaitForSecondsThen (patrol.walkForThisManySeconds, resetTimeout) );
			patrol.waiting = true;
		} if (!patrol.waitForTimeOutInPatrol) {
            debug("Generating new values for random patrol.");
            patrol.generateNewValues();
            patrol.waiting = false;
        }

        float movementForce = speed * horizontalForceMod;

        // only move LR if grounded
        if (!isGrounded && !gameObject.name.Contains("floater")) {
            movementForce = speed / 10;
            movement.x = 0;
        }

        switch (patrol.dir) {
            case 0:
                break;
            case -1:
                transform.forward = left;
                movement.x -= movementForce;
                break;
            case 1:
                transform.forward = right;
                movement.x += movementForce;
                break;
        }
    }

	private void changeDir(){
		canChangeDirOnWallHit = false;
		StartCoroutine( t.WaitForSecondsThen(2,	setCanChangeDirOnWallHitTrue ) );
		transform.forward = transform.forward * -1;
		patrol.dir *= -1;
	}

	private void setCanChangeDirOnWallHitTrue(){
		canChangeDirOnWallHit = true;
	}

    private void debug(string str) {
        if (debugPrint) {
            print(str);
        }
    }

    void CmdExplosion() {
        if (!exploded) {
            exploded = true;
            EZparticles ezParticles = GameObject.FindObjectOfType<EZparticles>();
            ezParticles.explosionLong(transform.position, 0);
            StartCoroutine(waitThenPlay());
        }
    }

    IEnumerator waitThenPlay() {
        ClipManager clipManager = GameObject.FindObjectOfType<ClipManager>();
        yield return new WaitForSeconds(0.4f); 
        clipManager.PlaySound((int)clip.explosionLong);
    }



}