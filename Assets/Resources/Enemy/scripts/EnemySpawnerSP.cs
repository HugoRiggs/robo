﻿using UnityEngine;
using System.Collections;

public class EnemySpawnerSP : MonoBehaviour {

	public GameObject EnemyPrefab;
	public GameObject[] ourSpawnees;

	public int maxEnemyCount = 3, spawnIntervalSeconds = 10;

	private Transform spawnTransform;
	private TimeDependentFunctionCalling t;


    void Start() {
        spawnTransform = gameObject.transform;
    }

    public int NumSpawneesStillLiving() {
        int numSpawneesStillLiving = ourSpawnees.Length;
        foreach (GameObject enemy in ourSpawnees)
            if (enemy == null)
                numSpawneesStillLiving -= 1;
        return numSpawneesStillLiving;
    }

	void InitSpawning(int count) {
		if (count < maxEnemyCount) {

            ourSpawnees[count] =
                (GameObject)Instantiate(
                       EnemyPrefab,
                       spawnTransform.position,
                       spawnTransform.rotation);

			StartCoroutine (t.WaitForSecondsThen (
              spawnIntervalSeconds, InitSpawning, count + 1) );
		} else
			return;
	}

	public void nEnemiesAtxSecondInterval(int enemyCount, int spawnIntervalSeconds){

		maxEnemyCount = enemyCount;
        this.spawnIntervalSeconds = spawnIntervalSeconds;
		ourSpawnees = new GameObject[maxEnemyCount];
		InitSpawning (0);
	}
}
