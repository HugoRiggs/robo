﻿using UnityEngine;
using System.Collections;

public class jumpTrigger : MonoBehaviour {


    private bool cooledDown = true;

    void OnTriggerStay(Collider col) {
        if (cooledDown && (col.transform.tag == "Terrain" || col.transform.tag == "Box" || col.transform.tag == "Player" || col.transform.tag == "Enemy")) {
            GetComponentInParent<EnemyController>().jump();
            StartCoroutine(jumpCooldown());
        } 
    }


    IEnumerator jumpCooldown() {
        cooledDown = false;
        yield return new WaitForSeconds(1);
        cooledDown = true;
    }



}