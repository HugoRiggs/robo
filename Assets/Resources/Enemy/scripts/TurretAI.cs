﻿using UnityEngine;
using System.Collections;

public class TurretAI : MonoBehaviour {
    public float maxRange;
    public GameObject shellPrefab;
	public Transform shellSpawn;
    public float fireRate = 1f;
    GameObject player;

    private bool reloading = false;

    public void fire() {
        if (!reloading) { 
            spawnShellAndGiveVelocity();
            reloading = true;
            StartCoroutine ( waitFor ( fireRate ));
        }
    }

    private void spawnShellAndGiveVelocity() {
		var shell = (GameObject)Instantiate(
            shellPrefab, shellSpawn.position, shellSpawn.rotation);
		shell.GetComponent<Rigidbody> ().velocity = 
            shell.transform.forward * 100 + gameObject.GetComponent<Rigidbody>().velocity;
    }

	IEnumerator waitFor(float seconds){
		yield return new WaitForSeconds (seconds);
		reloading = false;
	}

    private float offset = 0;// 17 and -11 are good offset values in editor
    private float offset1 = 0;
    private float offset2 = 0;
    public void aimAt(Transform body) {
        offset1 = Random.Range(3, 5);
        Vector3 aimAt = body.position + new Vector3(0, offset1, 0);
        Vector3 aimFrom = transform.position + new Vector3(0, offset2, 0);
        Vector3 gravityUp = (aimAt - transform.position).normalized;
        Vector3 bodyUp = body.forward;
        Quaternion rotationOfPlayer = body.rotation;
        Quaternion targetRotation =
            Quaternion.FromToRotation(
                bodyUp + new Vector3(0, offset, 0), gravityUp) * rotationOfPlayer; 

        transform.rotation = Quaternion.Slerp(body.rotation, targetRotation, 150 * Time.deltaTime);
    }


    IEnumerator waitOnPlayer() {
        while (player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForSeconds(2f);
        }
    }

    void Start() {
        StartCoroutine(waitOnPlayer());
    }

    void FixedUpdate() {
        if(player != null) { 
            //aimAt(player.transform);
            float distanceFromPlayer = Vector3.Distance(player.transform.position , transform.position);
            if( distanceFromPlayer < maxRange ) {
                aimAt(player.transform);
                fire();
            }
        }
    }
}