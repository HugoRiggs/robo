﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {

    public string name;

    public ChasePlayerAI chaseAI;

    public void startChaseAi() {
        try { 
            chaseAI = gameObject.GetComponent<ChasePlayerAI>();
        } catch {
            print("Exception: add a `ChaseAI` script to the game object.");
        }
    }

}
