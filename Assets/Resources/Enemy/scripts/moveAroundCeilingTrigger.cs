﻿using UnityEngine;
using System.Collections;

public class moveAroundCeilingTrigger : MonoBehaviour {

   void  OnTriggerStay(Collider col) {
        if (col.transform.tag == "JumpThrough") { // @todo move to its own class or rename this class
            GetComponentInParent<EnemyController>().moveAroundCeiling();
        }
    }

}
