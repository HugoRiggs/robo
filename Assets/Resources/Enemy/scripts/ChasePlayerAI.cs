﻿/**
*   The purpose of the ChaisePlayerAI 
*   script should be to 
*   identify the direction the enemy should move 
*/


using UnityEngine;
using System.Collections;
using System;

public class ChasePlayerAI : MonoBehaviour {

	private GameObject player;
    // Vectors
    private Vector3 playerVec3;
    public Vector3 moveDirection;

    // Components
    private Animator animator;
    private Rigidbody rb;

    // Booleans
    private bool chase, turning, switchSides = true, inRange = false;
    public bool playerOnRight, seesPlayer = false, playerAbove=false, verticalChasingEnabled;
            
    // Variables
    private float c;
    public float viewRange = 40;

    // Constant
    private int unit = 1;

    void Awake() {
        animator = GetComponent<Animator>();
        transform.rotation = Quaternion.LookRotation(new Vector3(90, 0, 0));
        playerOnRight = true;
        chase = false;
        switchSides = true;
        StartCoroutine(waitOnPlayer());
    }

    IEnumerator waitOnPlayer() {
        while (player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForSeconds(2f);
        }
    }

    void FixedUpdate() {
        scanForPlayer();
        try {
            playerVec3 = player.transform.position;
            if (!turning) {
                bool tmp = playerOnRight;
                playerOnRight = (playerVec3.x > transform.position.x) ? true : false;
                switchSides = (playerOnRight != tmp) ? true : false;
            } if (switchSides) {
                turning = true;
                if (c >= 1) { c = 0; }
                if (playerOnRight && switchSides && seesPlayer)
                    transform.forward = new Vector3(90, 0, 0);
                else if (!playerOnRight && switchSides && seesPlayer)
                    transform.forward = new Vector3(90, 0, 0) * -1;
                c += 0.05f;
                if (c >= 1) {
                    switchSides = false;
                    turning = false;
                }
            } if (playerVec3.y > transform.position.y-3)
                playerAbove = true;
            else
                playerAbove = false;
        } catch (Exception e) {
            //print("Exception " + e);
        }
        if (!seesPlayer)
            moveDirection = Vector3.zero;
        else
            moveDirection.x = unit;
    }

    void scanForPlayer() {
        float distanceFromPlayer =  // Get distance from palyer
            Vector3.Distance( transform.position,  FindObjectOfType<GameManager>().player.transform.position);    

        if (distanceFromPlayer < viewRange) {   // Check if its within range
            seesPlayer = true;      // If yes, we see player
            chase = true;           // and we can chase
        }
    }

    void OnTriggerExit(Collider o) {
        if (o.transform.tag == "Player") {
            chase = true;
            seesPlayer = false;

        }
    }

}
