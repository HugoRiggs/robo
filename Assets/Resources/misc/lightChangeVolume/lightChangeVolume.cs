﻿using UnityEngine;
using System.Collections;

public class lightChangeVolume : MonoBehaviour {

	private Light directional_light;
	private Transform light_transform;

	void Start () {
		directional_light = GetComponentInChildren<Light> ();
		light_transform = directional_light.transform;
	}

	void OnTriggerEnter(Collider other){ 
		if(other.transform.tag == "Player")
			light_transform.Rotate(
                new Vector3(
                    light_transform.rotation.x - 90,
                    light_transform.rotation.y,
                    light_transform.rotation.z
                    ));
	}
}
