﻿using UnityEngine;
using System.Collections;

public class BoxBehavior : MonoBehaviour {

    public float fallRate = 60f;
    Rigidbody rigidbody;

    void Start() {
        rigidbody = GetComponent<Rigidbody>();
    }

    void Update() {

        rigidbody.AddForce(-Vector3.up * Time.deltaTime * fallRate);
    }


    void OnTriggerEnter(Collider other) {
        Vector3 pos = other.transform.position;
        rigidbody.AddExplosionForce(1000, pos, 5);
    }
}