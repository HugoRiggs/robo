﻿using UnityEngine;
using System.Collections;

public class BouncePad : MonoBehaviour {

    public float bounceForce = 100f;

    void OnTriggerEnter(Collider other) {
        Vector3 pos = other.transform.position;
            StartCoroutine(launchPlayer(other.gameObject));

    }


    IEnumerator launchPlayer(GameObject ob) {
        CharacterController plyr = null;
        try {
            plyr = ob.GetComponent<CharacterController>();
        } catch { }
        if (plyr != null) { 
            for (int i = 0; i < 15; i++) {
                plyr.Move(Vector3.up * Time.deltaTime * bounceForce);
                yield return new WaitForFixedUpdate();
            }
                yield return null;
        }
    }

}
