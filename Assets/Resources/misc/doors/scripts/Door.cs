﻿using UnityEngine;
using System.Collections;

/**
 * The class Door is used to activate/deactivate
 * a physical obstruction to the progress of the player.
 */

public class Door : MonoBehaviour {

    Lever lever;


    public bool isActive() {

        return gameObject.activeSelf;
    }


    public void open() {

        if (!gameObject.activeSelf) { } else { 

            GameObject
                .FindGameObjectWithTag("EZparticles")
                .GetComponent<EZparticles>()
                .explosion(transform.position, 2);

            gameObject.SetActive(false);
        }
    }


    public void close() {

        if (gameObject.activeSelf) { } else {

            GameObject
                .FindGameObjectWithTag("EZparticles")
                .GetComponent<EZparticles>()
                .explosion(transform.position, 2);

            gameObject.SetActive(true);
        }
    }

}
