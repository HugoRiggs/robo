﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour {

    public Door door;
    public Portal portal;
    public InputControlStruct io;
    public bool state;
    public float activateRange;
    public float promptDuration;
    public string promptTextTurnOn;
    public string promptTextTurnOff;

    GameObject player;
    Animator animator;

    void Start() {
        state = false;
        activateRange = 7.74f;
        StartCoroutine(setPlayer());
        animator = GetComponent<Animator>();
    }

    IEnumerator setPlayer() {
        try {
            player = GameObject.FindGameObjectWithTag("Player");
            io = FindObjectOfType<InputControl>().io;
        } catch { }

        yield return new WaitForSeconds(2);

        if (player == null)
            StartCoroutine(setPlayer());
    }


    void FixedUpdate() {
        if (player != null) {
            if (Vector3.Distance(player.transform.position, transform.position) < activateRange
                && io.InteractInput()) {
                switchState();
                animator.SetBool("state", state);
                if (door != null) { 
                    if (state == true)
                        door.open();
                    if (state == false)
                        door.close();
                }
                if(portal != null) {
                    if (state == true)
                        portal.activate();
                    else
                        portal.deactivate();
                }
                if(promptTextTurnOn != "") {
                    if (state)
                        GameObject.FindWithTag("EZsplash").GetComponent<EZsplash>().displayText(promptTextTurnOn, 20);
                    else  {
                        GameObject.FindWithTag("EZsplash").GetComponent<EZsplash>().displayText(promptTextTurnOff, 20);
                    }

                    TimeDependentFunctionCalling t;
                    StartCoroutine(t.WaitForSecondsThen(promptDuration, killPrompt));
                }
            }
        }
    }

    private void killPrompt() {
        GameObject.FindWithTag("EZsplash").GetComponent<EZsplash>().displayText("");
    }

    private void switchState() {
        if (state)
            state = false;
        else
            state = true;
    }

}
