﻿using UnityEngine;
using System.Collections;

public class killVolume : MonoBehaviour {


    void OnTriggerEnter(Collider other) {

        switch (other.transform.tag) {
            case "Player":
                other.GetComponent<PlayerMovement>().Respawn();
                break;
            case "Enemy":
                other.GetComponent<EnemyController>().destroyEnemyObject();
                break;
            default:
                Destroy(other.gameObject);
                break;
        }
    }
}