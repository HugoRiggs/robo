﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/**
 * The portal class is used as a 
 * trigger detector which loads a new level 
 * when the player triggers it.
 * 
 */

public class Portal : MonoBehaviour {

    public int levelNumber;

    void OnTriggerEnter(Collider other) {
        splosion();

        print("Player entered portal : level " + levelNumber + " loading.");

        try {

            LoadingBarScript lbs =
                gameObject.GetComponent<LoadingBarScript>();

            lbs.LoadLevel(levelNumber);

        } catch (System.Exception e) {
            print("Failed to load level, check if there is a \nloading bar script attatched to this object.");
            print(e);
        }
    
    }

    public void activate() {
        splosion();
        gameObject.SetActive(true);
    }

    public void deactivate() {
        splosion();
        gameObject.SetActive(false);
    }

    void splosion() {
        GameObject
            .FindGameObjectWithTag("EZparticles")
            .GetComponent<EZparticles>()
            .explosion(transform.position, 2);
    }
}