﻿using UnityEngine;
using System.Collections;

public class Flicker : MonoBehaviour {

    void Start() {
        StartCoroutine(flicker());
    }

    IEnumerator flicker() {
        Light bulb = gameObject.GetComponentInChildren<Light>();
        while (true) { 
            for(int i = 0; i < 5; i++) {
                bulb.GetComponentInParent<MeshRenderer>().enabled = true;
                //bulb.gameObject.GetComponent<MeshRenderer>().enabled = true;
                bulb.enabled = true;
                yield return new WaitForSeconds(0.05f);
                bulb.enabled = false;
                //bulb.gameObject.GetComponent<MeshRenderer>().enabled = false;
                bulb.GetComponentInParent<MeshRenderer>().enabled = false;
                yield return new WaitForSeconds(0.2f);

            }
                yield return new WaitForSeconds(4f + Random.Range(0, 2));
        }
    }

}
