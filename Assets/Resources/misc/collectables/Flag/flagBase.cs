﻿using UnityEngine;
using System.Collections;

public class flagBase : MonoBehaviour {

    public bool teamBlue;
    public int teamsPoints;
    public GameObject flagPrefab;

    private GameObject flagInstance;
    private bool flagAtBase;


    void Start() {
        flagInstance = Instantiate(flagPrefab);
    }


    void OnTriggerExit(Collider other) {
        if(other.tag == "flag")
            if (other.GetComponent<flag>().teamBlue == teamBlue) {
                print("flag not at base");
                flagAtBase = false;
            }

    }


    void OnTriggerEnter(Collider other) {
        if (other.transform.tag == "flag") {
            /*
            if (!flagInstance.GetComponent<flag>().isPickedUp && teamBlue != other.GetComponent<flag>().teamBlue && other.GetComponent<flag>().isPickedUp) {
                other.GetComponent<flag>().isPickedUp = false;
                other.GetComponent<flag>().captureFlag(other.gameObject);
                print("player scored a point");
            }*/
            if (flagAtBase && other.GetComponent<flag>().teamBlue != teamBlue) {
                other.GetComponent<flag>().isPickedUp = false;
                other.GetComponent<flag>().captureFlag(other.gameObject);
                print("player scored a point");
            }

            if (other.GetComponent<flag>().teamBlue == teamBlue) {
                print("flag at base");
                flagAtBase = true;
            }
        }
    }
}
