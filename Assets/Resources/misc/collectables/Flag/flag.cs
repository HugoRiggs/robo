﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class flag : NetworkBehaviour {

    public GameObject flagBase;

    [SyncVar]
    public bool teamBlue;
    [SyncVar]
    public bool isPickedUp;

    public PlayerAttributes playerAttributes;

    GameObject carrierPlayer;
    private Vector3 startPosition;
    private Quaternion startRotation;

    void Start() {
        isPickedUp = false;
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

	void OnTriggerEnter(Collider other) {
	}

    public void attachFlagToPlayer(GameObject player) {
        carrierPlayer = player.gameObject;
        isPickedUp = true;
        transform.parent = player.transform;
        transform.eulerAngles = player.transform.eulerAngles;

        transform.position = player.transform.position;
        transform.Rotate(new Vector3(0, -90, 0));

        if(player.transform.forward.x < 0)
            transform.position = new Vector3 (player.transform.position.x+3, transform.position.y+10, transform.position.z);
        else
            transform.position = new Vector3 (player.transform.position.x-3, transform.position.y+10, transform.position.z);
    }

    public void respawn() {
        transform.parent = null;
        transform.position = startPosition;
        transform.rotation = startRotation;
    }

    public void captureFlag(GameObject flag) {
                print(carrierPlayer);
                playerAttributes = carrierPlayer.GetComponent<PlayerAttributes>();
                print(playerAttributes);
                playerAttributes.setScore(playerAttributes.getScore() + 1);


        string printStr = !teamBlue ? "Blue team captured the flag." : "Red team captured the flag.";
        print(printStr);
        flag.GetComponent<flag>().respawn();
        flag.GetComponent<flag>().CmdCTF(flag);
    }

   [Command] 
    void CmdCTF(GameObject flag) {
        flag.GetComponent<flag>().CmdRespawn();
    }

    [Command]
    void CmdRespawn() {
        transform.parent = null;
        transform.position = startPosition;
        transform.rotation = startRotation;
    }
/*
    void OnTrigger(Collider other) {

        if (other.transform.tag == "flag")    // Capture the flag gameplay mechanics for picking up the flag.
            if (teamBlue != other.GetComponent<PlayerMovement>().teamBlue && !isPickedUp) {
                isPickedUp  = true;
            } else if (other.transform.tag == "flag")    // CTF mechanics for delivering flag and earning a point.
                if (other.transform.gameObject.GetComponent<flag>().teamBlue == other.GetComponent<PlayerMovement>().teamBlue && isPickedUp) {
                    isPickedUp = false;
                    captureFlag(gameObject);
                    PlayerAttributes playerAttributes = other.GetComponent<PlayerAttributes>();
                    playerAttributes.setScore(playerAttributes.getScore() + 1);
                    print("player scored a point");
                } 
    }
*/
}
