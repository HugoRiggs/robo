﻿using UnityEngine;
using System.Collections;
//  http://www.freesound.org/
// http://freemusicarchive.org/


// added shorter explosion, 
// changed enumaration (old explosion is now explosionL)
// Global namespace enumerator for accessing the clips
enum clip {
	coinGrab,
	jetBoots,
	steps,
	jetPack,
	explosion,
    collisionHit,
	explosionLong,
    whip
}


public class ClipManager : MonoBehaviour {

    // The 'l_' indicates audio played in a loop. In the Unity Interface.
	public float l_jetPackVolume = 0.03f;
	public float l_footStepVolume = 0.02f;

    public float
        jump_clip_volume,
        collision_clip_volume,
        coin_grab_volume,
        explosion_volume,
        explosion_long_volume,
        whip_volume;

    private const float default_volume = 1;
		
	
	public AudioClip[] audioClips;

	private AudioSource[] _sources;
	
	private bool playingFootStep = false;
	private bool playingJetPack  = false;

	void Start(){
		_sources = new AudioSource[audioClips.Length];
		for(int i =0;i<audioClips.Length;i++){

            _sources[i] = gameObject.AddComponent<AudioSource>(); // create sources

            _sources[i].clip = audioClips[i]; // add clips to sources

            _sources[i].volume = default_volume;

            // Assign volume 
            switch ((clip)i) {
                case clip.coinGrab:
                    _sources[i].volume = coin_grab_volume;
                    print("audio for " + (clip)i + " set at volume " + _sources[i].volume);
                    break;
                case clip.jetBoots:
                    _sources[i].volume = jump_clip_volume;
                    print("audio for " + (clip)i + " set at volume " + _sources[i].volume);
                    break;
                case clip.explosion:
                    _sources[i].volume = explosion_volume;
                    print("audio for " + (clip)i + " set at volume " + _sources[i].volume);
                    break;
                case clip.collisionHit:
                    _sources[i].volume = collision_clip_volume;
                    print("audio for " + (clip)i + " set at volume " + _sources[i].volume);
                    break;
                case clip.explosionLong:
                    _sources[i].volume = explosion_long_volume;
                    _sources[i].pitch = 1.2f;
                    print("audio for " + (clip)i + " set at volume " + _sources[i].volume);
                    break;
            }
		}
	}

	public void PlaySound(int clipIdx) {
		if (clipIdx >= 0 && clipIdx < _sources.Length){
            print("audio for " + (clip)clipIdx + " at volume " + _sources[clipIdx].volume);
			_sources [clipIdx].Play ();
		}
	}

	
	public void footSteps() {
		if(!playingFootStep)
			StartCoroutine(footStepsIEnum());
	}

    // Using IEnumerator to enable waiting for a given length, 
    // which allows the audio clip to play entierly. 
    // This allows the seamless audio clip to play in a loop. 
	IEnumerator footStepsIEnum(){
		playingFootStep = true;
		_sources [(int)clip.steps].volume = l_footStepVolume;
		_sources [(int)clip.steps].Play();
		yield return new WaitForSeconds(audioClips[(int)clip.steps].length + 0.25f);
		playingFootStep = false;
	}
	
	
	// Player's jetpack noise, it was created by myself with my microphone. Along with a program called Audacity.
	public void jetPack() {
		if(!playingJetPack)
			StartCoroutine(jetPackIEnum());
	}

	
	public void stopJetPack(){
	  //    print("length " + _sources.Length);
		StopCoroutine(jetPackIEnum());
		_sources [(int)clip.jetPack].Stop();
		playingJetPack = false;
	}
	
	IEnumerator jetPackIEnum(){
		playingJetPack = true;
		_sources [(int)clip.jetPack].volume = l_jetPackVolume ;
		_sources [(int)clip.jetPack].Play();
		yield return new WaitForSeconds(audioClips[(int)clip.jetPack].length) ;
		playingJetPack = false;
	}

}


