﻿using UnityEngine;
using System.Collections;

public class EZparticles : MonoBehaviour {

    public float shellDamage = 25f;
    public float explosionForce = 50f, explosionRadius = 15f;
    public ParticleSystem[] explosionParticleFX;
    private ParticleSystem explosionParticleFXInstance;
    private ClipManager  clipManager;

    public void Awake() {
        clipManager = GameObject.FindGameObjectWithTag("EZaudio").GetComponentInChildren<ClipManager>();
    }



    public void explosion(Vector3 pos, int type) {
        //print("type " + type);
        explosionParticleFXInstance = Instantiate(explosionParticleFX[type]);
        explosionParticleFXInstance.transform.position = pos;
        explosionParticleFXInstance.Play();
        try {
            GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<ClipManager>().PlaySound((int)clip.explosion); 					// Audio effect.;
        } catch {
            print("failed to play explosion sound");
        }
        Destroy(explosionParticleFXInstance.gameObject, explosionParticleFXInstance.duration);
    }


    public void explosionLong(Vector3 pos, int type) {
        explosionParticleFXInstance = Instantiate(explosionParticleFX[type]); 
        explosionParticleFXInstance.transform.position = pos;
        explosionParticleFXInstance.Play();
        try { 
            GameObject.FindGameObjectWithTag ("Player").GetComponentInChildren<ClipManager> ().PlaySound((int)clip.explosionLong); 					// Audio effect.;
        }
        catch {
            print("failed to play explosion sound");
        }
        Destroy(explosionParticleFXInstance.gameObject, explosionParticleFXInstance.duration);
    }


}
