﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EZsplash : MonoBehaviour {

    public GameObject loadImage;
	public Text displayTextfield;
    private GameObject imageInstance;
    

    void Awake() {
        Time.timeScale = 1;
        print("timescale " + Time.timeScale);
        /*
        imageInstance = Instantiate(loadImage);
        imageInstance.transform.parent = transform;
        imageInstance.transform.Translate(800, 400, 0);
        HideLoadImage();
        */
    }

    void HideLoadImage() {
        imageInstance.SetActive(false);
    }

    void ShowLoadImage() {
        imageInstance.SetActive(true);
    }


    public void displayText(string txt) {
        displayTextfield.text = txt;
    }

	public void displayText(string txt, int size){
        displayTextfield.fontSize = size;
		displayTextfield.text = txt;
	}

	public void clearText(){
		displayTextfield.text = "";
	}
}
