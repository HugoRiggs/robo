﻿using UnityEngine;
using System.Collections;

public class MazeSpawner : MonoBehaviour {

	public Maze mazePrefab;
	
	private Maze mazeInstance;
	
	void Awake () {
		BeginGame();
	}
	
	private void BeginGame() {
		mazeInstance = Instantiate(mazePrefab) as Maze;
		mazeInstance.transform.position = transform.position;// new Vector3(transform.position.x, transform.position.y,transform.position.z);
		mazeInstance.Generate();
	}
	
	private void RestartGame() {
        Destroy(mazeInstance.gameObject);
        BeginGame();
    }
	
}
