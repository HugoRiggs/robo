﻿using UnityEngine;
using System.Collections;

public class sequenceBuilder : MonoBehaviour {

	public GameObject prefab;
	public int right=0, left=0, up=0, down=0;
	public float[] offsets = {0,0,0,0f}; 


	void Start () {

		float width = prefab.GetComponent<Renderer>().bounds.size.x, height = prefab.GetComponent<Renderer>().bounds.size.y;
		float offX= offsets[0] - offsets[1],offY=offsets[2] - offsets[3], posX=transform.position.x, posY=transform.position.y, posZ=transform.position.z;


		 for (int i = 0; i < right; i++)
		    Instantiate(prefab, new Vector3(i * (width + offX) + posX, posY + (i*offY), posZ), Quaternion.identity);

		 for (int i = 0; i < left; i++)
		    Instantiate(prefab, new Vector3(i * (-width + offX) + posX, posY + (i*offY), posZ), Quaternion.identity);

		 for (int i = 0; i < up; i++)
		    Instantiate(prefab, new Vector3(posX+(i*offX), i * (height + offY) + posY, posZ), Quaternion.identity);

		 for (int i = 0; i < down; i++)
		    Instantiate(prefab, new Vector3(posX + (i*offX), i * (-height + offY) + posY, posZ), Quaternion.identity);
		    
	}

}
