﻿/*
*  A script for controlling elevator's in Unity.
*  
*	 Directions of movement are determined like so:
*	 0 or -2 =still
*	 1=down
*	 2=up
*	 3=left
*	 4=right
*	 -1=drop player
*
*/


using UnityEngine;
using System.Collections;

public class Elevator : MonoBehaviour {

	public float maxVelocitySqrMg = 20;
	public float speed = 20;
	int[] table180 = { 2 , 1 , 4, 3};
	bool in_flip;
	public bool verticalElevator = true;
    public bool StartsUpOrRight = true;

    float xVal = 0, yVal = 0, zVal = 0;

	private Rigidbody rb;
	private int intSwitch=0;
	private Vector3 myPosition;
	private int t ;
	private CameraMovement cam;

	void Start (){
		rb = GetComponent<Rigidbody>();
		intSwitch = -2;
		in_flip = false;
		rb.constraints = RigidbodyConstraints.FreezeAll;
		rb.freezeRotation = true;
	}

	
	IEnumerator startCircuit (float seconds){
        // print("starting circuit");
        yield return new WaitForSeconds (seconds);
        if (intSwitch == -2 && verticalElevator)
            if (StartsUpOrRight)
                intSwitch = 2;
            else
                intSwitch = 1;
        else if (intSwitch == -2 && !verticalElevator)
            if (StartsUpOrRight)
                intSwitch = 4;
            else
                intSwitch = 3;
	}


	void FixedUpdate () {
		myPosition = transform.position;
		move(intSwitch);
	}


    void move(int dir) {
        switch (intSwitch) {
            case 0:
                rb.velocity = Vector3.zero;
                break;
            case 1: // Down
                xVal = 0;
                yVal = -1;
                zVal = 0;
                break;
            case 2: // Up
                xVal = 0;
                yVal = 1;
                zVal = 0;
                break;
            case 3: // Left
                xVal = -1;
                yVal = 0;
                zVal = 0;
                break;
            case 4: // Right
                xVal = 1;
                yVal = 0;
                zVal = 0;
                break;
            default:
                rb.velocity = Vector3.zero;
                break;
        }

        if (rb.velocity.sqrMagnitude < maxVelocitySqrMg && intSwitch != 0) {
            doTranslate(xVal, yVal, zVal);
        }
    }


    void OnTriggerEnter(Collider other) {
        if (other.transform.tag == "Player" && intSwitch == -2) {
            StartCoroutine(startCircuit(0.5f));
        } else if (other.transform.tag == "Player" && intSwitch == 1) { StartCoroutine(DoThisThenWaitForSecondsThenDoThat(setIntSwitch, 0.2f, dirRes)); }

        if (other.transform.tag == "cylinderPlatform" || other.transform.tag == "Terrain" || other.transform.tag == "Elevator" || other.transform.tag == "Enemy" && intSwitch != -2) {
            StartCoroutine(DoThisThenWaitForSecondsThenDoThat(setIntSwitch, 2f, dir180));
        } else if (other.transform.tag == "AxisSw")
            StartCoroutine(DoThisThenWaitForSecondsThenDoThat(setIntSwitch, 2f, dir90));
    }


    void OnTriggerStay(Collider other) {
        if (other.transform.tag == "Player") {
            other.transform.parent = transform;
        }
    }


    void OnTriggerExit(Collider other) {
        if (other.transform.tag == "Player") {
            other.transform.parent = null;
        }
    }


    void doTranslate(float x, float y, float z) {
        transform.Translate(speed * x * Time.deltaTime, speed * y* Time.deltaTime, speed * z* Time.deltaTime);
    }

	
    void setIntSwitch() {
        if (intSwitch > 0) 
            { t = intSwitch; }
        intSwitch = 0;
    }


    void dir180() {  // Changes direction of elevator 180 degrees.
        intSwitch = t;
        if (intSwitch-1>=0) 
        {
            intSwitch = table180 [intSwitch - 1];
        }
    }


    void dir90() {
        intSwitch = t;
        if (intSwitch+1<=3) 
        {
            intSwitch = table180 [intSwitch + 1];
        }
        else 
            intSwitch = table180 [intSwitch - 3];
    }

    void dirRes() {
        intSwitch = t;
    }

//    void OnCollisionEnter(Collision other){
//		rb.velocity = Vector3.zero;
//        if (other.transform.tag == "Player" && intSwitch == -2) { StartCoroutine(startCircuit(0.5f)); }
//       else if (other.transform.tag == "Player" && intSwitch == 1) { StartCoroutine(DoThisThenWaitForSecondsThenDoThat(setIntSwitch, 0.2f, dirRes)); }
//	}


        IEnumerator flip (){
		in_flip = true;
		for(int i =0;i<360;i++){
			yield return new WaitForSeconds (.1f);
		}
		in_flip = false;
	}


    public delegate void noArgsFunc();
	

    IEnumerator waitForSecondsThen(float seconds, noArgsFunc f) {
     	yield return new WaitForSeconds (seconds);
        f();
    }


    IEnumerator DoThisThenWaitForSecondsThenDoThat(noArgsFunc f1, float seconds, noArgsFunc f2) {
        f1();
        yield return new WaitForSeconds(seconds);
        f2();
    }

}
