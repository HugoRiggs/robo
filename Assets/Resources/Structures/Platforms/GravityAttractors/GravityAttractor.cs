﻿using UnityEngine;
using System.Collections;

public class GravityAttractor : MonoBehaviour {
	public float gravity = -100f;
	Vector3 attractorPosition;
	Vector3 contactNormal;
	Vector3 contactPoint;
	Collider CC;
	private RigidbodyConstraints rbcNONE, rbcNRY_NPZ;
	float d;


	public void Start() {
		attractorPosition = transform.position;
	}

	public bool Attract(Transform body) {

		d = Vector3.Distance(body.position, attractorPosition);
		CC = body.GetComponent<Collider>();
		float activateDistance = (gameObject.GetComponent<Collider> ().bounds.size.y)+1f;

		if(d < activateDistance ){
			Vector3 gravityUp = (body.position - transform.position).normalized; //Vector3 gravityUp = (body.position+(body.up*2) - (transform.position-transform.up)).normalized;  // Gravity up calculation
			Vector3 bodyUp = body.up;

			Debug.DrawLine(body.position, transform.position ,Color.black); // RED: from bound minimum to cylinder center

			body.GetComponent<CharacterController>().Move(gravityUp * gravity);
			Quaternion targetRotation = Quaternion.FromToRotation(bodyUp, gravityUp) * body.rotation;
			body.rotation = Quaternion.Slerp(body.rotation, targetRotation, 150 * Time.deltaTime);
			return true;
		}
		return false;
	}

	void OnCollisionEnter(Collision collision) {
		ContactPoint contact = collision.contacts[0];
		contactNormal = contact.normal;
		contactPoint = contact.point;
	}
}
