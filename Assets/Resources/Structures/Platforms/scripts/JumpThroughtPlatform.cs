﻿using UnityEngine;
using System.Collections;

public class JumpThroughtPlatform : MonoBehaviour {

    BoxCollider bc;
    GameObject player;
    InputControlStruct io;

    void Start() {
        bc = GetComponent<BoxCollider>();
        StartCoroutine( setPlayer() );

    }


    IEnumerator setPlayer() {

        try {
            player = GameObject.FindGameObjectWithTag("Player");
        } catch { }

        yield return new WaitForSeconds(2);

        if (player == null)
            StartCoroutine(setPlayer());

    }

    void FixedUpdate() {
        if(player != null) {
            if (player.transform.position.y < transform.position.y) {
                bc.enabled = false;
            } else
                bc.enabled = true;

            if (io.isCrouching()) {
                bc.enabled = false;
            }
        }
    }


}
