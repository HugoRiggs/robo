﻿#pragma strict

public var rotateX : boolean = true;
public var rotateY : boolean = false;
public var rotateZ : boolean = false;
public var speedModifier : float = 1;

private var x : float = 0;
private var y : float = 0;
private var z : float = 0;

function Update () {
var c = speedModifier;
	if(Time.deltaTime !=0){
		if(rotateX)
	        if(x < 360)
	            x += 0.1 * c;
	        else
	            x = 0;


	    if(rotateY)
	        if(y < 360)
	            y += 0.1 * c;
	        else
	            y = 0;


	    if(rotateZ)
	        if(z < 360)
	            z += 0.1 * c;
	        else
	            z = 0;

	    
		gameObject.transform.eulerAngles = Vector3(x,y,z);
	}
}