﻿using UnityEngine;
using System.Collections;

public class ItemRotation : MonoBehaviour
{

    public float rotPerMin;

    void Update()
    {
        transform.Rotate(0, (float)6.0 * rotPerMin * Time.deltaTime, 0);
    }
}
