﻿/* An inherited set of methods
 * for controlling movement
 */

using UnityEngine;
using System.Collections;


public class MovementController: MonoBehaviour  {

    public float fallingRateUnit = 1f;
    public float fallingRateDefault = 1f;
    public float fallingRateMax = 60;    // max fallingRate can go to
    public float fallingRate = 1;

    private CharacterController cc;
    private Vector3 movement = new Vector3(0, 0, 0);
    private float Zvalue = 0;
    private bool jumping = false;

    void Awake() {
        cc = GetComponent<CharacterController>();
    }

    private void reCenter() {
        cc.transform.position =  // Reset to our starting Z value. 
            new Vector3(cc.transform.position.x, cc.transform.position.y, Zvalue);
    }

    public void setZvalue(float value) { Zvalue = value; }

    public void resetMovement() {
        reCenter();
        movement = new Vector3(0, 0, 0);
    }

    public void moveUp(float force) {
        movement.y += force;
    }

    public void moveDown(float force) {
        movement.y -= force;
    }

    public void moveRight(float force) {
        movement.x += force;
    }

    public void moveLeft(float force) {
        movement.x -= force;
    }

    public bool isGrounded() { return cc.isGrounded; }


    public void applyGravity() {
        float gravityForce = GameObject.FindGameObjectWithTag("EZgravity").GetComponent<EZgravity>().getForce();
        if (!cc.isGrounded && fallingRate < fallingRateMax)
            fallingRate += fallingRateUnit;
        if (cc.isGrounded)
            fallingRate = fallingRateDefault;
        moveDown(gravityForce * Mathf.Pow(fallingRate, 2));
    }

    public float velocityX() {
        return cc.velocity.x;
    }

    public float velocityY() {
        return cc.velocity.y;
    }

    public void jump() {
        if (!jumping) { 
            jumping = true;
            StartCoroutine(jumpUpForceOverTime());
        }
    }

    IEnumerator jumpUpForceOverTime() {
        for (int i = 0; i < 15; i++) {
            yield return new WaitForFixedUpdate();
        }
        jumping = false;
        yield return null;
    }

    public bool isJumping() { return jumping; }

    public void applyMovement() {
        cc.Move(movement * Time.deltaTime);
    }


}
