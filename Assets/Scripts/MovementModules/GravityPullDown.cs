﻿using UnityEngine;
using System.Collections;

public class GravityPullDown : MonoBehaviour {

    public float fallingRateUnit = 1f;
    public float fallingRateDefault = 1f;
    public float fallingRateMax = 60;    // max fallingRate can go to
    public float fallingRate = 1;
    public bool isGrounded;
    public float distToGround;

    private Vector3 movement;


    void Start() {
        //        RaycastHit hit;       // works if object is starting on ground level
        //        if (Physics.Raycast (transform.position, -Vector3.up, out hit)) {
        //            distToGround = hit.distance;
        //        } 
        distToGround = 0.5f*gameObject.GetComponent<BoxCollider>().bounds.size.y;//size.y;
    }

    void FixedUpdate() {
        isGrounded = IsGrounded();
        movement = new Vector3(0, 0, 0);
        float gravityForce = GameObject.FindGameObjectWithTag("EZgravity").GetComponent<EZgravity>().getForce();
        if (!IsGrounded() && fallingRate < fallingRateMax)
            fallingRate += fallingRateUnit;
        if (IsGrounded())
            fallingRate = fallingRateDefault;
        movement.y -= gravityForce * Mathf.Pow(fallingRate, 2);
        gameObject.GetComponent<Rigidbody>().AddForce(movement * Time.deltaTime);
    }

    private bool IsGrounded() {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }
}
