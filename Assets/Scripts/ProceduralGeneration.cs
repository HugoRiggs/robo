﻿using UnityEngine;
using System.Collections;

public class ProceduralGeneration : MonoBehaviour {

    public int numberPlatforms = 10;
    public GameObject platform1;
    public Vector3 currentV3 = new Vector3(0, 0, 0);
    public Vector3 newV3 = new Vector3(0, 0, 0);
    public Transform platformRotation;

	// Use this for initialization
	void Start () {
       // currentV3 = spawnNewPlatform(platform1, currentV3);

        for (int i = 0; i < numberPlatforms; i++)
        {
            currentV3 = spawnNewPlatform(platform1, currentV3);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}


    public Vector3 randomVector3Generator(Vector3 currentV3)
    {
        Vector3 random = new Vector3(Random.Range(currentV3.x + 10, currentV3.x + 40), Random.Range(currentV3.y - 10, currentV3.y + 10), 0);
        return random;
    }

    public Vector3 spawnNewPlatform(GameObject spawnObject, Vector3 oldLocation)
    {
        Vector3 newLocation = randomVector3Generator(oldLocation);
        Instantiate(spawnObject, newLocation , Quaternion.identity);
        return newLocation;
    }

}
