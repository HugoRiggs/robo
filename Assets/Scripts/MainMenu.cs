﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{


    public GUISkin mySkin;
    public float delayBetweenFocusChanges = .5f;
    public string gameName;

    private Rect[] myRects = new Rect[2];
    private string[] mainMenuLabels = new string[2];
    private JoystickButtonMenu mainMenu;

    private int currentlyPressedButton = -1;

    void Start()
    {
    
        myRects[0] = new Rect(Screen.width - (Screen.width * (float)0.25), Screen.height - (Screen.height * (float)0.25) - 80, 150, 50);
        myRects[1] = new Rect(Screen.width - (Screen.width * (float)0.25), Screen.height - (Screen.height * (float)0.25), 150, 50);

        mainMenuLabels[0] = "New Game";
        mainMenuLabels[1] = "Exit";
        

        mainMenu = new JoystickButtonMenu(2, myRects, mainMenuLabels, "Fire1", JoystickButtonMenu.JoyAxis.Vertical);
        
    }

    void OnGUI()
    {
        GUI.skin = mySkin;

        
        mainMenu.DisplayButtons();
    }

    void Update()
    {
        if (mainMenu.enabled)
        {
            if (mainMenu.CheckJoystickAxis())
            {
                Invoke("Delay", delayBetweenFocusChanges);
            }
            currentlyPressedButton = mainMenu.CheckJoystickButton();

            switch (currentlyPressedButton)
            {
                case 0:
                    SceneManager.LoadScene("01 First Level");
                    return;
                case 1:
                    Application.Quit();
                    return;
            }
        }
    }

    private void Delay()
    {
        mainMenu.isCheckingJoy = false;
    }
}