﻿using UnityEngine;
using System.Collections;

public class ObjectZAxisAlign : MonoBehaviour {

    private float positionZ;
	// Use this for initialization
	void Start () {
        positionZ = GameObject.FindObjectOfType<GameManager>().getGeneralZaxis();
        StartCoroutine(align());
	}


    public IEnumerator align() {
        GameObject player = null; // declare a player
        while (player == null) {
            player = GameObject.FindGameObjectWithTag("Player"); // select the player
            yield return new WaitForSeconds(3f);
        }
        positionZ = player.transform.position.z; // get z positional value of player
        setPosition(new Vector3(transform.position.x, transform.position.y, positionZ)); // set our positional value to player's
    }


	void setPosition (Vector3 pos) {
        transform.position = pos; // set object position to passed position
 //       Transform[] children = gameObject.GetAllComponentsInChildren<Transform>();

 //       // set all childrens z positions
 //       foreach (Transform child in children) {
 //           child.transform.position = new Vector3(
 //            child.transform.position.x,
 //            child.transform.position.y,
 //               pos.z
 //               );
 //       }

	}
}
