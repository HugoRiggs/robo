﻿using UnityEngine;
using System.Collections;

public struct TimeDependentFunctionCalling {

    public delegate void noArgsFunc();
	public delegate void oneIntArgFunc(int x);
    public delegate void oneBoolArgFunc(bool arg);
	public delegate void gameObjectArgFunc(GameObject obj);

    /*
    * Functions which take no arguments.
    */
    public IEnumerator WaitForSecondsThen(float seconds, noArgsFunc f) {
      yield return new WaitForSeconds (seconds);
        f();
    }


    public IEnumerator DoThisThenWaitForSecondsThenDoThat(noArgsFunc f1, float seconds, noArgsFunc f2) {
        f1();
        yield return new WaitForSeconds(seconds);
        f2();
    }

    /*
    * Functions which take a bool argument.
    */
    public IEnumerator WaitForSecondsThen(float seconds, oneBoolArgFunc f, bool arg) {
      yield return new WaitForSeconds (seconds);
      f(arg);
    }

	/*
    * Functions which take an int argument.
    */
	public IEnumerator WaitForSecondsThen(float seconds, oneIntArgFunc f, int x) {
		yield return new WaitForSeconds (seconds);
		f(x);
	}

	/*
    * Functions which take a gameobject.
    */
	public IEnumerator WaitForSecondsThen(float seconds, gameObjectArgFunc f, GameObject obj) {
		yield return new WaitForSeconds (seconds);
		f(obj);
	}
}
