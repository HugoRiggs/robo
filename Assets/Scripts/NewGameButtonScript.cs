﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NewGameButtonScript : MonoBehaviour {

    //This script is used to load new levels using buttons

    public string firstScene = "";

    // Use this for initialization
    void Start() {

    }


    void update() {
        //Start new game if A is pressed
        if (Input.GetButtonDown("Submit")) {
            Debug.Log("The A button is being pressed");
            LoadNewScene(firstScene);
        }
    }

    public void LoadNewScene(string sceneName)
    {
        print("Scene by the name of " + sceneName + " has been loaded");
        SceneManager.LoadScene(sceneName);
    }


}
