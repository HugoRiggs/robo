﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Doorway : MonoBehaviour {

    public string sceneName;

    // Use this for initialization
    void Start () {

        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider collide)
    {
        if (Input.GetKey(KeyCode.E) || Input.GetKey("joystick button 2"))
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
