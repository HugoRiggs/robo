# Robo #

#### game software in pre-alpha stage


Robo is a side scroller fighter-platformer, __[Gameplay trailer](https://www.youtube.com/watch?v=VXrA_-CNS_Y&feature=youtu.be)__
  the game features a mechanical player who can make use of various abilities:

* jump jets, gliding
* crouch, jump
* explosive projectiles, whip
* gameplay altering platforms (trampolines, elevators)

Majority of source code is in the C# language, Microsofts programming language. The language C# is a close relative to Java, as a computer language. Shorter scripts often appearing as the mock Javascript* AKA C# script.


Folder heirarchy is still being implemented, however the  vision is for it to be like such:

## Prefabricated objects
```
$TopDir/Assets/Resources/< some game component>/< descriptive sub-dirs>/< prefabricated object>
```

## Scripts for a prefabricated object
```
$TopDir/Assets/Resources/< some game component>/< descriptive sub-dirs>/< prefabricated object>/scripts
```

### In this repository we ###

* Host our current development build of robo.
* stage is pre-alpha

### How do I get set up? ###

* Install  [Unity](https://unity3d.com/)
* Unity is bundled with with a C# compilers and IDE
* Dependencies - C#
* Database configuration - none
* How to run tests - no tests implemented
* Deployment instructions - build through the Unity interface, run the .exe

### Contribution guidelines ###

* Writing tests - Usually I test code by just compiling and running the game I never tried unit tests in Unity and never found any support for such testing.
* Code review - none yet
* Other guidelines - none yet

### Music selection from

https://soundcloud.com/creativecommonsmusicfree

https://www.youtube.com/audiolibrary/music

###### * Unity does not use pure Javascript, but instead an intepreted functional language which resembles Javascript. Perhaps better known as "C# script," this  language utilizes a C# intepreter.
